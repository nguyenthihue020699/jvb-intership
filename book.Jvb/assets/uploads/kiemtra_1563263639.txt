﻿1. Tạo form đăng ký gồm : Email, Name, Password, Birthday, Phone
2. Tạo form đăng nhập : Email, Password
3. Thiết kế Database
4. Thực hiện đăng ký : validate dữ liệu ( validate bằng cả javascript + php) : 
	Email: định dạng email, không được để trống, là duy nhất trong Database
	Name: tối thiểu 3 ký tự , tối đa 30 ký tự, Không được để trống
	Password: tối thiểu 8 ký tự, tối đa 32 ký tự, có chữ hoa, có ký hiệu đặc biệt, Không được để trống
	Phone: Định dạng số điện thoại

	Nếu có lỗi, hiển thị form đăng ký + thông báo lỗi cho người dùng + Hiển thị dữ liệu nhập trước đó

5. Thực hiện đăng nhập:
	
	Đăng nhập không thành công -> thông báo lỗi
	Đăng nhập không thành công quá 5 lần -> cho phép đăng nhập sau 2 phút 
	Đăng nhập thành công -> Hiển thị thông tin người dùng