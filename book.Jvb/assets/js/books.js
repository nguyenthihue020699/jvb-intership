(function ($) {
	'use strict';

	/*Slider*/
    runSlider();
    var slider;
    function runSlider(){
        slider = setInterval(activeSlider, 5000);
    }
    function activeSlider(next){
        var images = document.getElementsByClassName('slider-item');
        var totalImges = images.length;
        var currentID = 0;
        for(var i = 0; i< totalImges; i++){
            if(images[i].classList.contains('active')){
                currentID = images[i].attributes['data-id'].value;
                break;
            }
        }
        if(!next){
            if(currentID > 0){
                var nextSlider = parseInt(currentID)-1;
            }else{
                var nextSlider = totalImges-1;
            }
        }else{
            if(currentID<1){
                var nextSlider = parseInt(currentID) + 1;
            }else {
                nextSlider = 0;
            }
        }

        images[nextSlider].classList.add("active");
        images[currentID].classList.remove("active");

    }

    //Tất cả những cái trong này sẽ được thực hiện sau khi page load xong
    $(document).ready(function () {
		//Xóa cart item
		$('.delete-book').click(function (e) {
			e.preventDefault();
			var c = confirm('Bạn có muốn xóa không?');
			if(c === false){
				return;
			}
			var t = $(this);
			var cartID = t.data('id');
			var divMessage = $('.ue-message');
            divMessage.html('');
            $.ajax({
                url: site_url + 'cart/remove_cart_ajax',
                type: 'POST',
                dataType: 'json',
                data: {
                    cart_id: cartID,
                },
                success: function (data) {
					if(data.status === true){
						t.closest('tr').remove();
						$('.list-product span').text(data.count);
                        $('.ue-total-price').html(data.new_price);
					}
                    divMessage.html(data.message);
                    t.find('.loading').hide();
                }

            });
        });

		//Update cart + price khi thay đổi số lượng
		/*$('.update-quantity').change(function () {
			var t = $(this);
            var cartID = t.data('id');
            var numberChange = t.val();
            var divMessage = $('.ue-message');
            divMessage.html('');

            $.ajax({
                url: site_url + 'cart/update_cart_ajax',
                type: 'POST',
                dataType: 'json',
                data: {
                    cart_id: cartID,
					quantity: numberChange
                },
                beforeSend: function () {
                    t.find('.loading').show();
                },
                success: function (data) {
                    if(data.status === true){
						$('.ue-total-price').html(data.new_price);
                    }
                    divMessage.html(data.message);
                    t.find('.loading').hide();
                }

            });
        });*/
    });
})(jQuery);
