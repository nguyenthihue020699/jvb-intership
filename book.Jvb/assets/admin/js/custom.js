(function($){
    'use strict';

    $('.btn-delete').click(function (e) {
        var deleteConf = confirm("Bạn có muốn xóa không?");
        if(!deleteConf){
            e.preventDefault();
        }
    });
     $(document).ready(function () {
		//Xóa cart item
		$('.delete-book').click(function (e) {
			e.preventDefault();
			var t = $(this);
			var bookID = t.data('id');
           	console.log(bookID);
			var divMessage = $('.ue-message');
            divMessage.html('');
            $.ajax({
                url: site_url + 'book/delete_book_ajax',
                type: 'POST',
                dataType: 'json',
                data: {
                    book_id: bookID,
                },
                beforeSend: function () {
                  
                },
                success: function (data) {
					if(data.status === true){
						t.closest('tr').remove();
					}
                }

            });
        });
    });
})(jQuery);