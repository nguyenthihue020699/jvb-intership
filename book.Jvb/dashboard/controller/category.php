<?php
class Category extends Base_controller{
	public function __construct(){
		parent:: __construct();
	}

	public function add_Category(){
		if(isset($_POST['add_category'])){
			/*echo(1233);die;*/
			$err = array();

			$name = UE_Input::post('name');
			if(empty($name)){
				array_push($err, 'Tên danh mục không được trống');
			}
			if(!empty($err)){
				$err_str = implode('<br />', $err);
				UE_Message::add($err_str, 'message', 'danger');
			}else{
				$post_data = $_POST;
				$thumb = $this->uploadFile();
					if(!empty($thumb)){
						$post_data['thumb'] = 'uploads/' . $thumb;
					}

				$res = $this->model->addCategory($post_data);
				if($res > 0){
				//$message =  "Thanh cong";
					UE_Message::add('Thêm mới thành công', 'message', 'success');
				}else{
					UE_Message::add('Thêm mới thất bại', 'message', 'danger');
				}
			}
		}
		$this->loadView('category/addCategory');
	}
	public function uploadFile(){
		if(!empty($_FILES) && isset($_FILES['thumb'])){
			$uploadDir = SITEPATH . 'assets/uploads/';
			$file_name = $_FILES['thumb']['name'];
			$file_type = $_FILES['thumb']['type'];
			$file_size = $_FILES['thumb']['size'];
			$file_temp = $_FILES['thumb']['tmp_name'];

			$ext = pathinfo($file_name, PATHINFO_EXTENSION);

			$file_name = str_replace('.' . $ext, '', $file_name);

			$file_name = $file_name  . '_' . time() . '.' . $ext;


			$target_file = $uploadDir . basename($file_name);

			if (move_uploaded_file($file_temp, $target_file)) {
		    	return $file_name;
		    }else{
		    	return '';
		    }
		}
	}
	public function listCategory(){
		$data = $this->model->getCategory();
		$this->loadView('category/category_view', array('res' => $data));
	}
	/*public function getIdCategory(){
		$data = $this->model->getIDCategory();
		$this->loadView('book/addBook', array('res' => $data));
	}*/
	public function delete($category_id){
		if(!empty($category_id)){
			$res = $this->model->delete($category_id);
			if($res > 0){
				UE_Message::add('Bạn đã xóa thành công', 'category', 'success');
			}else{
				UE_Message::add('Đã có lỗi xảy ra', 'category', 'warning');
			}
		}
		header('location: ' . ue_get_admin_link('category', 'listCategory'));
	}
	public function update($category_id){
		if(isset($_POST['name'])){
			$post_data = $_POST;

			if(isset($_FILES['thumb']) && !empty($_FILES['thumb']['name'])){
			$thumb = $this->uploadFile();
				if(!empty($thumb)){
					$post_data['thumb'] = 'uploads/' . $thumb;
				}
			}else{
				$post_data['thumb'] = $post_data['thumb_temp'];
			}
			$res = $this->model->update($category_id, $post_data);
			if($res > 0){
				UE_Message::add('Cập nhật thành công', 'cate', 'success');
			}else{
				UE_Message::add('Cập nhật thất bại', 'cate', 'warning');
			}
		}

		$currentCate = $this->model->getCategoryByID($category_id);
		$this->loadView('category/edit', array('cate' => $currentCate));
	}
}
?>