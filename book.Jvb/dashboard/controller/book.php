
<?php
class Book extends Base_Controller{
	public function __construct() {
		parent::__construct();
	}

	public function index(){
		$data = $this->model->getAllBooks();
		$this->loadView('book/index', array('data' => $data));
	}

	public function delete($book_id){
		if(ue_is_login() && !empty($book_id)){
			$role = ue_get_role();
			if(in_array($role, array(0, 1))){
				$author_book = $this->model->getAuthorOfBook($book_id);
				$current_user = ue_get_user_data();
				if(($author_book['author'] == $current_user['id']) || $role == 0){
					$res = $this->model->delete($book_id);
					if($res > 0){
						UE_Message::add('Bạn đã xóa thành công', 'book', 'success');
					}else{
						UE_Message::add('Đã có lỗi xảy ra', 'book', 'warning');
					}
					header('location: ' . ue_get_admin_link('book', 'index'));
				}else{
					UE_Message::add('Bạn không phải người tạo quyển sách này', 'book', 'warning');
					header('location: ' . ue_get_admin_link('book', 'index'));
				}
			}else {
				header('location: ' . ue_get_admin_link('book', 'index'));
			}
		}
	}
	public function delete_book_ajax(){
		if(ue_is_login()){
			$book_id = UE_Input::post( 'book_id' );
			$role = ue_get_role();
			if(in_array($role, array(0, 1))){
				$author_book = $this->model->getAuthorOfBook($book_id);
				$current_user = ue_get_user_data();
				if(($author_book['author'] == $current_user['id']) || $role == 0){
					$res = $this->model->delete($book_id);

				}
				$res = [
				'status' => true,
				'message' => '<div class="alert success">Xóa thành công</div>',
				'count' => $count,
				/*'new_price' => ue_format_price($total_price)*/
			];
			echo json_encode($res);
			die;
			}
		}
	}
	public function add(){
		if(ue_is_login() && in_array(ue_get_role(), array(0, 1))){
			if(isset($_POST['add_book'])){
				$err = array();
				$name = UE_Input::post('name');
				if(empty($name)){
					array_push($err , 'Tên sách không được trống');
				}
				if(!empty($err)){
					$err_str = implode('<br />', $err);
					UE_Message::add($err_str, 'message', 'danger');
				}else{
					$post_data = $_POST;

					$current_user = ue_get_user_data();
					$post_data['author'] = $current_user['id'];

					$thumb = $this->uploadFile();
					if(!empty($thumb)){
						$post_data['thumb'] = 'uploads/' . $thumb;
					}

					$res = $this->model->add($post_data);
					if($res > 0){
						UE_Message::add('Thêm mới thành công', 'message', 'success');
					}else{
						UE_Message::add('Thêm mới thất bại', 'message', 'danger');
					}
				}
			}
			$this->loadModel('Category');
			$cate_data = $this->model->getCategory();
			$this->loadView('book/add', array('data' => $cate_data));
		}else{
			header('location: ' . ue_get_admin_link('book', 'index'));
		}
	}

	public function uploadFile(){
		if(!empty($_FILES) && isset($_FILES['thumb'])){
			$uploadDir = SITEPATH . 'assets/uploads/';
			$file_name = $_FILES['thumb']['name'];
			$file_type = $_FILES['thumb']['type'];
			$file_size = $_FILES['thumb']['size'];
			$file_temp = $_FILES['thumb']['tmp_name'];

			$ext = pathinfo($file_name, PATHINFO_EXTENSION);

			$file_name = str_replace('.' . $ext, '', $file_name);

			$file_name = $file_name  . '_' . time() . '.' . $ext;


			$target_file = $uploadDir . basename($file_name);

			if (move_uploaded_file($file_temp, $target_file)) {
		    	return $file_name;
		    }else{
		    	return '';
		    }
		}
	}

	public function edit($book_id){
		if(isset($_POST['name'])){
			$post_data = $_POST;

			if(isset($_FILES['thumb']) && !empty($_FILES['thumb']['name'])){
				$thumb = $this->uploadFile();
				if(!empty($thumb)){
					$post_data['thumb'] = 'uploads/' . $thumb;
				}
			}else{
				$post_data['thumb'] = $post_data['thumb_temp'];
			}
				
			$res = $this->model->update($book_id, $post_data);
			if($res > 0){
				UE_Message::add('Cập nhật thành công', 'book', 'success');
			}else{
				UE_Message::add('Cập nhật thất bại', 'book', 'warning');
			}
		}

		$currentBook = $this->model->getBookByID($book_id);
		$this->loadModel('Category');
		$cate_data = $this->model->getCategory();
		$this->loadView('book/edit', array('book' => $currentBook, 'cate' => $cate_data));
	}
}