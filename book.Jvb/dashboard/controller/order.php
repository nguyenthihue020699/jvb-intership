<?php
class Order extends Base_Controller{
	public function __construct() {
		parent::__construct();
	}
	public function listOrder(){
		/*dd(12333);die;*/
		$data = $this->model->getAllOrder();
		$this->loadView('order/index', array('data' => $data));
	}
	public function delete($order_id){
		if(!empty($order_id)){
			$res = $this->model->delete($order_id);
			if($res > 0){
				UE_Message::add('Bạn đã xóa thành công', 'category', 'success');
			}else{
				UE_Message::add('Đã có lỗi xảy ra', 'category', 'warning');
			}
		}
		header('location: ' . ue_get_admin_link('order', 'listOrder'));
	}
}
?>