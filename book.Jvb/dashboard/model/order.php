<?php
class Order_Model extends Base_Model{
	public function __construct(){
		parent:: __construct();
	}
	public function getAllOrder(){
		$sql = "SELECT * FROM book_order";
		$query = mysqli_query($this->conn, $sql);
		$result = array();
		if(!empty($query) && $query->num_rows > 0){
			while ($row = mysqli_fetch_assoc($query)) {
				$result[] = $row;
			}
		}
		return $result;
	}
	public function delete($order_id){
		$sql = "DELETE FROM book_order WHERE id = $order_id";
		$query = mysqli_query($this->conn, $sql);
		return $query;
	}
}
?>