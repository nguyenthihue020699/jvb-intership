
<?php
class Category_Model extends Base_Model{
	public function __construct(){
		parent:: __construct();
	}
	public function getCategory(){
		$sql = 'SELECT * FROM categories';
		$query = mysqli_query($this->conn, $sql);
		$result = array();
		if($query->num_rows > 0){
			while ($row = mysqli_fetch_assoc($query)) {
				# code...
				$result[] = $row;
			}
		}
		return $result;
	}
	
	public function addCategory($data){
		$sql = "INSERT INTO categories(name, thumb, description) VALUES('{$data['name']}', '{$data['thumb']}', '{$data['description']}')";
		/*dd($sql);die;*/
		$query = mysqli_query($this->conn, $sql);
		return $query;
	}
	public function delete($category_id){
		$sql = "DELETE FROM categories WHERE id = $category_id";
		$query = mysqli_query($this->conn, $sql);
		return $query;
	}

	public function update($category_id, $data){
		$sql = "UPDATE categories SET name = '{$data['name']}', thumb = '{$data['thumb']}', description = '{$data['description']}'  WHERE id = $category_id";
		$query = mysqli_query($this->conn, $sql);
		return $query;
	}

	public function getCategoryByID($category_id){
		$sql = "SELECT * FROM categories WHERE id = $category_id";
		$query = mysqli_query($this->conn, $sql);
	
		$result = array();
		if(!empty($query) && $query->num_rows > 0){
			while ($row = mysqli_fetch_assoc($query)) {
				$result = $row;
			}
		}
	
		return $result;
	}

}
?>