<?php
class Book_Model extends Base_Model{
	public function __construct(){
		parent:: __construct();
	}

	public function delete($book_id){
		$sql = "DELETE FROM books WHERE id = $book_id";
		$query = mysqli_query($this->conn, $sql);
		return $query;
	}
	
	public function getAllBooks(){
		$role = ue_get_role();
		$where = '';
		if($role == 1){
			$data_user = ue_get_user_data();
			$user_id = $data_user['id'];
			$where = ' AND author = ' . $user_id;
		}

		$sql = 'SELECT books.*, users.id as user_id, users.fullname as author FROM books LEFT JOIN users ON books.author = users.id WHERE 1=1 '. $where .' order by books.id DESC';
		$query = mysqli_query($this->conn, $sql);
		$result = array();
		if(!empty($query) && $query->num_rows > 0){
			while ($row = mysqli_fetch_assoc($query)) {
				$result[] = $row;
			}
		}
		return $result;
	}
	public function add($data){
		$sql = "INSERT INTO books(name, description, price, number_page, publish_year, publish_author, thumb, category_id, author) VALUES('{$data['name']}', '{$data['description']}', '{$data['price']}', '{$data['number_page']}', '{$data['publish_year']}', '{$data['publish_author']}', '{$data['thumb']}', '{$data['category_id']}', {$data['author']})";
		/*dd($sql);die;*/
		$query = mysqli_query($this->conn, $sql);
		/*dd($query);die;*/
		return $query;
	}

	public function getBookByID($book_id){
		$sql = "SELECT * FROM books WHERE id = {$book_id}";
		$query = mysqli_query($this->conn, $sql);
		$result = array();
		if(!empty($query) && $query->num_rows > 0){
			while ($row = mysqli_fetch_assoc($query)) {
				$result = $row;
			}
		}
		return $result;
	}

	public function update($book_id, $data){
		$sql = "UPDATE books SET thumb = '{$data['thumb']}', name = '{$data['name']}', description='{$data['description']}', price = '{$data['price']}', number_page={$data['number_page']}, publish_year={$data['publish_year']}, publish_author='{$data['publish_author']}', category_id={$data['category_id']} WHERE id = $book_id";

		$query = mysqli_query($this->conn, $sql);
		return $query;
	}

	public function getAuthorOfBook($book_id){
		$sql = "SELECT author FROM books WHERE id = {$book_id}";
		$query = mysqli_query($this->conn, $sql);
		$result = array();
		if(!empty($query) && $query->num_rows > 0){
			while ($row = mysqli_fetch_assoc($query)) {
				$result = $row;
			}
		}
		return $result;
	}
}
?>