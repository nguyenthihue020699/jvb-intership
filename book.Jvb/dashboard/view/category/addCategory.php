<?php
$role = ue_get_role();
if(!in_array($role, array(0))){
	header('Location:' . SITEURL);
	exit();
}
?>
<h4>Thêm danh mục</h4>
<form action="" method="post" enctype="multipart/form-data">
  <div class="form-group">
  	<label for="exampleInputEmail1" class="label mt-5">*Ảnh*</label>
    <input type="file"  name="thumb">
    <br />
    <label for="exampleInputEmail1" class="label mt-3" >*Tên danh mục*</label>
    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tên danh mục" name="name">
    <label for="exampleInput10" class="label mt-3">*Mô tả*</label>
    <input type="text" class="form-control" id="exampleInputEmail10" aria-describedby="emailHelp" placeholder="Mô tả" name="description">
  </div>
   <?php UE_Message::show('message'); ?>
   <br/>
  <button type="submit" class="btn btn-primary" name="add_category">Thêm</button>
 
</form>