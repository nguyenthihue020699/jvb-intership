<?php
$role = ue_get_role();
if(!in_array($role, array(0))){
	header('Location:' . SITEURL);
	exit();
}
if(!empty($cate)){
	?>
	<h4>Sửa danh mục</h4>
	<form action="" method="post" enctype="multipart/form-data">
	  <div class="form-group">
	  	<label for="exampleInputEmail" class="label mt-5">Ảnh</label>
	  	<img src="<?php echo SITEURL . 'assets/' . $cate['thumb']; ?>" width="50" height="50" />
    	<input type="file"  name="thumb">
    	<input type="hidden" name="thumb_temp" value="<?php echo $cate['thumb']; ?>">
    	<br />
	    <label for="exampleInputEmail1" class="label mt-3">Tên danh mục</label>
	    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tên danh mục" name="name" value="<?php echo UE_Input::post('name', $cate['name']); ?>">
	    <label for="exampleInputEmail2" class="label mt-3">Mô tả</label>
	    <input type="text" class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp" placeholder="Mô tả" name="description" value="<?php echo UE_Input::post('description', $cate['description']); ?>">
	  </div>
	   <?php UE_Message::show('cate'); ?>
	   <br/>
	  <button type="submit" class="btn btn-primary" name="add_category">Sửa</button>
	 
	</form>
	<?php
}else{
	?>
	<div class="alert alert-danger mt-4">Không tồn tại danh mục này.</div>
	<?php
}
