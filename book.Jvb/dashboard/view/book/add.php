<?php
$role = ue_get_role();
if(!in_array($role, array(0,1))){
	header('Location:' . SITEURL);
	exit();
}
?>
<h4 class="mt-2">Thêm sách</h4>
<form action="" method="post" enctype="multipart/form-data">
  <div class="form-group">
    <label for="exampleInputEmail1" class="label mt-5">*Ảnh*</label>
    <input type="file"  name="thumb">
    <br />
    <label for="exampleInputEmail2" class="label mt-3">*Tên sách*</label>
    <input type="text" class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp" placeholder="Tên sách" name="name">
    <label for="exampleInputEmail3" class="label mt-3">*Giá*</label>
    <input type="text" class="form-control" id="exampleInputEmail3" aria-describedby="emailHelp" placeholder="Giá" name="price">
    <label for="exampleInputEmail4" class="label mt-3">*Số trang*</label>
    <input type="text" class="form-control" id="exampleInputEmail4" aria-describedby="emailHelp" placeholder="Số trang" name="number_page">
    <label for="exampleInputEmail5" class="label mt-3">*Năm xuất bản*</label>
    <input type="text" class="form-control" id="exampleInputEmail5" aria-describedby="emailHelp" placeholder="Năm xuất bản" name="publish_year">
    <label for="exampleInputEmail6" class="label mt-3">*Nhà xuất bản*</label>
    <input type="text" class="form-control" id="exampleInputEmail6" aria-describedby="emailHelp" placeholder="Nhà xuất bản" name="publish_author">
    <label for="exampleInputEmailDanhMUc" class="label mt-3">*Danh mục*</label>
    <!-- <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Danh mục" name="category_id"> -->
    <select name="category_id" id="exampleInputEmailDanhMUc" class="form-control">
      <?php
        if(!empty($data)){
            foreach ($data as $k => $v) {
              ?>
                <option value="<?php echo $v['id']; ?>"> <?php echo $v['name']; ?> </option>
              <?php
            }
        }

      ?>
    </select>
    </br>
     <label for="exampleInputEmail7" class="label mt-3">*Mô tả*</label>
    <textarea type="text" class="form-control" id="exampleInputEmail7" rows="5" aria-describedby="emailHelp" placeholder="Mô tả" name="description"></textarea>
  </div>
   <?php UE_Message::show('message'); ?>
   <br/>
  <button type="submit" class="btn btn-primary mb-10" name="add_book">Thêm</button>
 
</form>