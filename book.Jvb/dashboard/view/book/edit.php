
<h4>Sửa thông tin sách</h4>
<?php
$role = ue_get_role();
if(!in_array($role, array(0,1))){
	header('Location:' . SITEURL);
	exit();
}
if(!empty($book)) {
	?>
    <form action="" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="exampleInputEmail1" class="label mt-5">Ảnh</label>
            <img src="<?php echo SITEURL . 'assets/' . $book['thumb']; ?>" width="50" height="50" />
            <input type="file" name="thumb">
            <input type="hidden" name="thumb_temp" value="<?php echo $book['thumb']; ?>">
            <br />
            <label for="exampleInputEmail2" class="label mt-3">Tên sách</label>
            <input type="text" class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp"
                   placeholder="Tên sách" name="name" value="<?php echo UE_Input::post( 'name', $book['name'] ); ?>">

            <label for="exampleInputEmail3" class="label mt-3">Giá</label>
            <input type="text" class="form-control" id="exampleInputEmail3" aria-describedby="emailHelp"
                   placeholder="Giá" name="price" value="<?php echo UE_Input::post( 'price', $book['price'] ); ?>">

            <label for="exampleInputEmail4" class="label mt-3">Số trang</label>
            <input type="text" class="form-control" id="exampleInputEmail4" aria-describedby="emailHelp"
                   placeholder="Số trang" name="number_page"
                   value="<?php echo UE_Input::post( 'number_page', $book['number_page'] ); ?>">

            <label for="exampleInputEmail5" class="label mt-3">Năm xuất bản</label>
            <input type="text" class="form-control" id="exampleInputEmail5" aria-describedby="emailHelp"
                   placeholder="Năm xuất bản" name="publish_year"
                   value="<?php echo UE_Input::post( 'publish_year', $book['publish_year'] ); ?>">

            <label for="exampleInputEmail6" class="label mt-3">Nhà xuất bản</label>
            <input type="text" class="form-control" id="exampleInputEmail6" aria-describedby="emailHelp"
                   placeholder="Nhà xuất bản" name="publish_author"
                   value="<?php echo UE_Input::post( 'publish_author', $book['publish_author'] ); ?>">

            <label for="exampleInputEmailDanhMUc" class="label mt-3">Danh mục</label>
            <select name="category_id" id="exampleInputEmailDanhMUc" class="form-control">

    
				<?php
				if ( ! empty( $cate ) ) {
					foreach ( $cate as $k => $v ) {
						$selected     = '';
						$current_cate = UE_Input::post( 'category_id', $book['category_id'] );
						if ( $current_cate == $v['id'] ) {
							$selected = 'selected';
						}
						?>
                        <option value="<?php echo $v['id']; ?>" <?php echo $selected; ?>> <?php echo $v['name']; ?> </option>
						<?php
					}
				}

				?>
            </select>
            <br/>
            <label for="exampleInputEmail7" class="label mt-3">Mô tả</label>
            <textarea type="text" class="form-control" id="exampleInputEmail7" rows="5" aria-describedby="emailHelp"
                      placeholder="Mô tả"
                      name="description"><?php echo UE_Input::post( 'description', $book['description'] ); ?></textarea>
        </div>
		<?php UE_Message::show( 'message' ); ?>
        <br/>
        <button type="submit" class="btn btn-primary" name="add_book">Thêm</button>

    </form>
	<?php
}else{
    ?>
    <div class="alert alert-warning mt-4">Không tồn tại quyển sách này</div>
    <?php
}