<?php
$role = ue_get_role();
if(!in_array($role, array(0,1))){
	header('Location:' . SITEURL);
	exit();
}
?>
<div class="table-book mt-4">
	<h3 class="mb-4">Sách <a href="<?php echo ue_get_admin_link('book','add'); ?>" class="btn btn-info btn-sm float-right">Thêm mới</a></h3>
	<?php
	if(!empty($data)) {
		UE_Message::show('book');
		?>
		<table class="table">
			<thead>
			<tr>
				<th scope="col">Ảnh</th>
				<th scope="col">Tên</th>
				<th scope="col">Giá</th>
                <th scope="col">Người tạo</th>
				<th scope="col">Nhà xuất bản</th>
				<th scope="col">Hành động</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($data as $k => $v){ ?>
			<tr>
				<td><img src="<?php echo SITEURL . '/assets/' . $v['thumb']; ?>" width="50" height="50"/></td>
				<td><?php echo $v['name']; ?></td>
				<td><?php echo ue_format_price($v['price']); ?></td>
                <td><?php echo !empty($v['author']) ? $v['author'] : '---'; ?></td>
				<td><?php echo $v['publish_author']; ?></td>
				<td>
					<a href="<?php echo ue_get_admin_link('book', 'edit') . '/' . $v['id']; ?>" class="btn btn-info btn-sm">Sửa</a>
					<a href="<?php echo ue_get_admin_link('book', 'delete') . '/' . $v['id']; ?>" class="btn btn-danger btn-sm btn-delete delete-book" data-id="<?php echo $v['id']; ?>">Xóa</a>
				</td>
			</tr>
			<?php } ?>
			</tbody>
		</table>
		<?php
	}else{
		?>
		<div class="alert alert-warning">Không có quyển sách nào.</div>
		<?php
	}
	?>
</div>