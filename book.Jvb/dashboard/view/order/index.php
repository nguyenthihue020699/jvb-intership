<?php
$role = ue_get_role();
if(!in_array($role, array(0,1))){
	header('Location:' . SITEURL);
	exit();
}
?>
<div class="table-book mt-4">
	<h3 class="mb-4">Hóa đơn</h3>
	<?php
	if(!empty($data)) {
		UE_Message::show('book');
		?>
		<table class="table">
			<thead>
			<tr>
				<th scope="col">ID_Người mua</th>
				<th scope="col">Tên người mua</th>
				<th scope="col">Tổng tiền</th>
				<th scope="col">Hành động</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($data as $k => $v){ ?>
			<tr>
				<td><?php echo $v['user_id']; ?></td>
				<td><?php echo $v['user_name']; ?></td>
				<td><?php echo $v['price']; ?></td>
				<td>
					<a href="<?php echo ue_get_admin_link('order', 'delete') . '/' . $v['id']; ?>" class="btn btn-danger btn-sm btn-delete">Xóa</a>
				</td>
			</tr>
			<?php } ?>
			</tbody>
		</table>
		<?php
	}else{
		?>
		<div class="alert alert-warning">Không có hóa đơn nào.</div>
		<?php
	}
	?>
</div>