<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Sách</title>
	<link href="<?php echo SITEURL; ?>assets/plugins/fontawesome/css/all.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo SITEURL; ?>assets/css/books.css">
	<link rel="stylesheet" type="text/css" href="<?php echo SITEURL; ?>assets/css/login.css">
    <script>
        var site_url = '<?php echo SITEURL; ?>';
    </script>
</head>
<body>

	<div class="header">
		<div class="header1">
			<a href="<?php echo SITEURL; ?>"><img src="<?php echo SITEURL; ?>assets/images/logomaumoi.png" width="200" height="50"></a>
			<div class="search">
				<ul>
					<li class="get">
						<form method="GET" action="<?php echo ue_get_link('book', 'search'); ?>">
							<input class="search1" type="text" name="s" placeholder="Tìm kiếm" required="" value="<?php echo isset($_GET['s']) ? $_GET['s'] : ''; ?>">
							<input class="button" type="submit" value="Tìm">
						</form>
					</li>
					<li><a href="<?php echo ue_get_link('cart', 'detail') ?>"><i class="fas fa-shopping-cart"></i>
						<?php
						if(!empty(UE_Input::get_session('cart'))){
							echo '(' . count(UE_Input::get_session('cart')) . ')';
						}
						?>
					</a></li>
					<?php 
						if(!ue_is_login()){
					?>
						<li><a href="#"><i class="fa fa-user" aria-hidden="true"></i> Tài khoản</a>
							<ul class="login">
								<li><a href="<?php echo ue_get_link('user', 'login'); ?>">Đăng nhập</a></li>
								<li><a href="<?php echo ue_get_link('user', 'signup'); ?>">Đăng ký</a></li>
							</ul>

						</li>
					<?php
						}
					?>
					
					<li><a href="#"><?php
    					if(ue_is_login()){
    				  	 	$user_data = ue_get_user_data();
       						echo 'Xin chào, ' . $user_data['fullname'];
    					}	
    				?></a>
    				<ul class="logout1">
    					<li><a href="<?php echo SITEURL . '	dashboard'; ?>">Trang quản trị</a></li>
    					<li><a href="<?php echo ue_get_link('user', 'logout'); ?>">Đăng xuất</a></li>
    				</ul>

    				</li>
				</ul>
			</div>
		</div>
		<div class="service">
				<div class="nav-header">

				<div class="container">

				<ul>
					<?php
					if(!empty($menus)){

						foreach ($menus as $k => $v) {
							?>
							<li class="nav">
								<a href="<?php echo $v['link']; ?>"><?php echo $v['name']; ?></a>
								<?php
								if(isset($v['child']) && !empty($v['child'])){
									?>
									<ul class="sub-menu">
									<?php
										foreach ($v['child'] as $kk => $vv) {
									?>
								<li>
									<a href="<?php echo $vv['link']; ?>"><?php echo $vv['name']; ?></a>
								</li>
									<?php
										}
									?>
									</ul>
								<?php
								}
							?>
							</li>
							<?php
						}
					}
				?>
				</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="body">
		<div class="slider">
			<img class="item active slider-item " src="<?php echo SITEURL; ?>assets/images/Banner_1920-95__Hti_s ch___3_.jpg" data-id="0">
			<img class="item slider-item " src="<?php echo SITEURL; ?>assets/images/header_Sinh-nhat.jpg" data-id="1">
		</div>
	</div>
<div class="content">
    <div class="container">