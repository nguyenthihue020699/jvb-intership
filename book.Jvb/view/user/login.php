<?php
if(ue_is_login()){
    header('location: ' . SITEURL);
    exit();
}
?>
<div class="login">
	<h2 class="title">Đăng nhập</h2>
	<form method="post" action="">
		<div class="log">
			<h1><a href="#"><img src="<?php echo SITEURL; ?>assets/images/logomaumoi.png" width="200" height="50"></a></h1>
			<input type="text" name="username" placeholder="Tài khoản" >
			<input type="password" name="password" placeholder="Mật khẩu" >
			<?php
	            if(isset($errors)){
		            ?>
                    <div class="alert alert-danger small"><?php echo $errors; ?></div>
		            <?php
	            }
	            if(isset($message)){
		            ?>
                   	<div class="alert alert-success small"><?php echo $message; ?></div>
                   	
		            <?php
	            }
	        ?>
			<button name="login_action">Đăng nhập</button>
		</div>
	</form>
</div>