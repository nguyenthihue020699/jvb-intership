<?php
class Cart extends Base_Controller {
	public function __construct() {
		parent::__construct();
	}

	public function add_cart(){
		if(ue_is_login()){
			if(isset($_POST['add_to_cart'])){
				/*dd($_POST['add_to_cart']);die;*/
				$book_id = UE_Input::post('book_id');
				$this->loadModel('Book');
				$book_data = $this->model->getBookByID($book_id);
				if(!empty($book_data)){
					$book_data = array_shift($book_data);
					if(!isset($_SESSION['cart'][$book_data['id']]['number'])){
						$number = 1;
					}else{
						$number = $_SESSION['cart'][$book_data['id']]['number'] + 1;
					}

					$book_data['number'] = $number;
					/*dd($book_data);die;*/
					$_SESSION['cart'][$book_data['id']] = $book_data;
				}
			}
			header('location: ' . ue_get_link('cart', 'detail'));
		}else {
			header('location: ' . SITEURL);
		}
	}

	public function detail(){
		$this->loadView('book/cart');
	}

	//Remove Cart using ajax
	public function remove_cart_ajax(){
		if(ue_is_login()) {
			$book_id = UE_Input::post( 'cart_id' );
			if(!empty($book_id)){
				if(isset($_SESSION['cart']) && isset($_SESSION['cart'][$book_id])){
					unset($_SESSION['cart'][$book_id]);
				}
			}
			$count = count($_SESSION['cart']);
			$total_price = ue_get_cart_total_price();
			$res = [
				'status' => true,
				'message' => '<div class="alert success">Xóa thành công</div>',
				'count' => $count,
				'new_price' => ue_format_price($total_price)
			];
			echo json_encode($res);
			die;
		}else{
			$res = [
				'status' => false,
				'message' => '<div class="alert danger">Đã có lỗi xảy ra</div>'
			];
			echo json_encode($res);
			die;
		}
	}

	public function remove(){
		if(ue_is_login()){
			$book_id = UE_Input::get('book_id');
			if(!empty($book_id)){
				if(isset($_SESSION['cart']) && isset($_SESSION['cart'][$book_id])){
					unset($_SESSION['cart'][$book_id]);//remove theo id của cart (theo id của sách)
				}
			}
			header('location: ' . ue_get_link('cart', 'detail'));
		}
	}

	//Update cart using ajax
	/*public function update_cart_ajax(){
		if(ue_is_login()) {
			$book_id = UE_Input::post( 'cart_id' );
			$quantity = UE_Input::post( 'quantity' );
			if(!empty($book_id) && $quantity){
				//7
				//3
				$res = UE_Input::get_session('cart');
				if(isset($res[$book_id])){
					$res[$book_id]['number'] = $quantity;
					$_SESSION['cart'] = $res;
					$total_price = ue_get_cart_total_price();
					$res = [
						'status' => true,
						'message' => '<div class="alert success">Cập nhật thành công</div>',
						'new_price' => ue_format_price($total_price)
					];
					echo json_encode($res);
					die;
				}else{
					$res = [
						'status' => false,
						'message' => '<div class="alert danger">Đã có lỗi xảy ra</div>'
					];
					echo json_encode($res);
					die;
				}
			}else{
				$res = [
					'status' => false,
					'message' => '<div class="alert danger">Đã có lỗi xảy ra</div>'
				];
				echo json_encode($res);
				die;
			}
		}else{
			$res = [
				'status' => false,
				'message' => '<div class="alert danger">Đã có lỗi xảy ra</div>'
			];
			echo json_encode($res);
			die;
		}
	}*/

	public function update(){
		if(isset($_POST['update_cart'])){
			$number = UE_Input::post('number');
			$book_id = UE_Input::post('book_id');
			$cart = UE_Input::get_session('cart');
			if(!empty($book_id)) {
				foreach ( $book_id as $k => $v ) {
					if ( isset( $cart[ $v ] ) ) {
						$_SESSION['cart'][ $v ]['number'] = $number[ $k ];
					}
				}
			}
		}
		header('location: ' . ue_get_link('cart', 'detail'));
	}
}