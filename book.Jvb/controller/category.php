<?php
class Category extends Base_controller{
	public function __construct(){
		parent:: __construct();
	}

	public function detail($cate_id){
		$data = $this->model->getCategoryByID($cate_id);
		$this->loadView('category/detail', array('res' => $data));
	}
	public function introduce(){
		$data = $this->model->getAllCategory();
		$this->loadView('page/introduce', array('res' => $data));
	}
	/*public function introduce(){
		$this->loadView('page/introduce');
	}*/
}
?>