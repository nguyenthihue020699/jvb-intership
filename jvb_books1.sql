-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th7 11, 2019 lúc 04:27 PM
-- Phiên bản máy phục vụ: 10.1.34-MariaDB
-- Phiên bản PHP: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `jvb_books`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `books`
--

CREATE TABLE `books` (
  `id` int(9) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `number_page` int(9) NOT NULL,
  `publish_year` int(5) NOT NULL,
  `publish_author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `books`
--

INSERT INTO `books` (`id`, `name`, `description`, `price`, `number_page`, `publish_year`, `publish_author`, `thumb`, `category_id`) VALUES
(1, 'Quyển 1', 'Bài viết giới thiệu về một cuốn sách thường mang tính chất mô tả, cung cấp thông tin cần thiết về cuốn sách đó. Loại bài viết này được thực hiện bằng cách người viết nêu rõ các ý tưởng, thông điệp hay mục đích của tác giả muốn truyền tải đến người đọc mà mình cảm nhận được khi đọc sách', '150000', 156, 2019, 'Nam Cao', 'images/p1.jpg', 6),
(2, 'Quyển 2', 'Sách không chỉ là nguồn tri thức vô tận đối với mỗi người mà ngày càng có nhiều minh chứng khoa học cho thấy những tác động rất tốt của việc đọc sách đối với sự phát triển cả về thể chất lẫn trí tuệ và tâm hồn của trẻ.', '100000', 567, 2018, 'Kim Lân', 'images/p2.jpg', 8),
(3, 'Quyển 3', 'Đây là phần chính của bài giới thiệu. Yêu cầu chung của phần này là phải (1) khái quát, tóm tắt được nội dung chủ đề tác phẩm, (2) nêu được các giá trị về nội dung của tác phẩm đối với xã hội và bạn đọc.', '80000', 127, 2009, 'Tô Hoài', 'images/p6.jpg', 5),
(4, 'Quyển 4', '“Hà Nội băm sáu phố phường” là tập bút kí nổi tiếng của nhà văn Thạch Lam, tập hợp lại những bài viết in trên báo sau khi ông qua đời, do Nhà xuất bản Văn học phát hành.', '135000', 343, 2000, 'Vũ Trọng Phụng', 'images/p8.jpg', 8),
(5, 'Quyển 5', 'Sách không chỉ là nguồn tri thức vô tận đối với mỗi người mà ngày càng có nhiều minh chứng khoa học cho thấy những tác động rất tốt của việc đọc sách đối với sự phát triển cả về thể chất lẫn trí tuệ và tâm hồn của trẻ.', '56000', 158, 2004, 'Nam Cao', 'images/p7.jpg', 5),
(7, 'Quyển 6', 'Có thể nói, trong lý luận cũng như thực tiễn biên mục mô tả Việt Nam đã có nhiều trăn trở để đi đến thống nhất trong quy tắc biên mục mô tả nói chung và phương pháp lập TĐMT nói riêng', '108000', 167, 1998, 'Kim Đồng', 'images/p8.jpg', 6),
(8, 'Quyển 7', 'Nếu bạn muốn đọc cuốn sách này và quan tâm đến những nhân vật trong đó và ngốn từng con chữ như thể đó là kem và đồng thời muốn vừa hụt hẫng vừa vui mừng trong cùng một trang sách, thì cuốn này dành cho bạn.', '250000', 180, 2016, 'Hồng Đức', 'images/p6.jpg', 8),
(9, 'Quyển 8', '“Một lời nhắn đặc biệt cho những ai nói nhận xét của tôi làm họ ngưng đọc cuốn sách này lại: Không không không! Đọc đi! Tôi thực sự đã đọc lại toàn bộ series vào hè năm ngoái và thích nó ngay lập tức. Hãy đọc vì nó: lố bịch, viết hay, hài hước, VÔ LÍ một cách thú vị. Đừng mong nó là cuốn tiểu thuyết tuyệt vời nhất và bạn sẽ ổn thôi.”', '109000', 256, 2001, 'Thanh niên', 'images/p1.jpg', 6),
(10, 'Quyển 9', 'Đây, đây, đây là một trong những cuốn đó. Là cuốn đã giành lại niềm tin của tôi với văn học, khiến tôi không ngừng ngạc nhiên bởi sức hấp dẫn, ám ảnh và tuyệt vời của nó. Tôi biết mình luôn bị nói là khoa trương, nhưng đây thực sự là một trong những cuốn hay nhất mà tôi từng đọc.”', '158000', 206, 1995, 'Chính trị Quốc gia', 'images/p3.jpg', 5),
(11, 'Quyển 10', '“Dành cho bất cứ ai đang tự hỏi liệu họ có nên tiếp tục đọc cuốn này không, bởi những người nhận xét đều chia 2 phe (YÊU hoặc GHÉT nó): dù sao thì, Ừ. Đọc đi! Nhưng: nếu bạn thấy mình đang ghét nó trong tầm 100 trang, hãy bỏ nó xuống và đi đi. Bởi nó sẽ không hay lên được. (Thực sự. Hãy làm vì tôi nói và đừng vì tôi làm: Đặt. Nó. Xuống.) Còn cho những ai YÊU cuốn sách này: Tuyệt! Chúc mừng bạn. Chẳng có gì tốt đẹp hơn cuốn sách mà bạn thích cả!”', '204000', 347, 2018, 'Hồng Đức', 'images/p4.jpg', 5),
(12, 'Quyển 11', '“Cây đa Bác Hồ với muôn ngả đường xuân” là tên tập ký sự của tác giả Vương Tâm - Do Nhà xuất bản Văn học ấn hành. Với hơn 30 bài viết giới thiệu trong tập sách, tác giả cho bạn đọc thấy sự suy ngẫm của mình về tư tưởng của Chủ tịch Hồ Chí Minh trong việc phát động Tết trồng cây; những lời dạy sâu sắc của Bác về giá trị của việc trồng cây xanh cho tương lai và cho sự thân thiện của con người với thiên nhiên.', '456000', 255, 2016, 'Công an nhân dân', 'images/p5.jpg', 7);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `book_order`
--

CREATE TABLE `book_order` (
  `id` int(9) NOT NULL,
  `book_data` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(9) NOT NULL,
  `price` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `book_order`
--

INSERT INTO `book_order` (`id`, `book_data`, `user_id`, `price`, `status`) VALUES
(7, 'a:1:{i:2;a:10:{s:2:\"id\";s:1:\"2\";s:4:\"name\";s:9:\"Quyển 2\";s:11:\"description\";s:301:\"Sách không chỉ là nguồn tri thức vô tận đối với mỗi người mà ngày càng có nhiều minh chứng khoa học cho thấy những tác động rất tốt của việc đọc sách đối với sự phát triển cả về thể chất lẫn trí tuệ và tâm hồn của trẻ.\";s:5:\"price\";s:6:\"100000\";s:11:\"number_page\";s:3:\"567\";s:12:\"publish_year\";s:4:\"2018\";s:14:\"publish_author\";s:8:\"Kim Lân\";s:5:\"thumb\";s:13:\"images/p2.jpg\";s:11:\"category_id\";s:1:\"2\";s:6:\"number\";s:1:\"1\";}}', 1, '100000', 'complete'),
(8, 'a:2:{i:5;a:10:{s:2:\"id\";s:1:\"5\";s:4:\"name\";s:9:\"Quyển 5\";s:11:\"description\";s:301:\"Sách không chỉ là nguồn tri thức vô tận đối với mỗi người mà ngày càng có nhiều minh chứng khoa học cho thấy những tác động rất tốt của việc đọc sách đối với sự phát triển cả về thể chất lẫn trí tuệ và tâm hồn của trẻ.\";s:5:\"price\";s:5:\"56000\";s:11:\"number_page\";s:3:\"158\";s:12:\"publish_year\";s:4:\"2004\";s:14:\"publish_author\";s:7:\"Nam Cao\";s:5:\"thumb\";s:13:\"images/p7.jpg\";s:11:\"category_id\";s:1:\"5\";s:6:\"number\";s:1:\"7\";}i:4;a:10:{s:2:\"id\";s:1:\"4\";s:4:\"name\";s:9:\"Quyển 4\";s:11:\"description\";s:225:\"“Hà Nội băm sáu phố phường” là tập bút kí nổi tiếng của nhà văn Thạch Lam, tập hợp lại những bài viết in trên báo sau khi ông qua đời, do Nhà xuất bản Văn học phát hành.\";s:5:\"price\";s:6:\"135000\";s:11:\"number_page\";s:3:\"343\";s:12:\"publish_year\";s:4:\"2000\";s:14:\"publish_author\";s:19:\"Vũ Trọng Phụng\";s:5:\"thumb\";s:13:\"images/p8.jpg\";s:11:\"category_id\";s:1:\"4\";s:6:\"number\";s:1:\"1\";}}', 1, '527000', 'complete'),
(9, 'a:3:{i:5;a:10:{s:2:\"id\";s:1:\"5\";s:4:\"name\";s:9:\"Quyển 5\";s:11:\"description\";s:301:\"Sách không chỉ là nguồn tri thức vô tận đối với mỗi người mà ngày càng có nhiều minh chứng khoa học cho thấy những tác động rất tốt của việc đọc sách đối với sự phát triển cả về thể chất lẫn trí tuệ và tâm hồn của trẻ.\";s:5:\"price\";s:5:\"56000\";s:11:\"number_page\";s:3:\"158\";s:12:\"publish_year\";s:4:\"2004\";s:14:\"publish_author\";s:7:\"Nam Cao\";s:5:\"thumb\";s:13:\"images/p7.jpg\";s:11:\"category_id\";s:1:\"5\";s:6:\"number\";s:1:\"7\";}i:4;a:10:{s:2:\"id\";s:1:\"4\";s:4:\"name\";s:9:\"Quyển 4\";s:11:\"description\";s:225:\"“Hà Nội băm sáu phố phường” là tập bút kí nổi tiếng của nhà văn Thạch Lam, tập hợp lại những bài viết in trên báo sau khi ông qua đời, do Nhà xuất bản Văn học phát hành.\";s:5:\"price\";s:6:\"135000\";s:11:\"number_page\";s:3:\"343\";s:12:\"publish_year\";s:4:\"2000\";s:14:\"publish_author\";s:19:\"Vũ Trọng Phụng\";s:5:\"thumb\";s:13:\"images/p8.jpg\";s:11:\"category_id\";s:1:\"4\";s:6:\"number\";s:1:\"1\";}i:6;a:10:{s:2:\"id\";s:1:\"6\";s:4:\"name\";s:9:\"Quyển 6\";s:11:\"description\";s:282:\"Đây là phần chính của bài giới thiệu. Yêu cầu chung của phần này là phải (1) khái quát, tóm tắt được nội dung chủ đề tác phẩm, (2) nêu được các giá trị về nội dung của tác phẩm đối với xã hội và bạn đọc.\r\n\r\n\";s:5:\"price\";s:6:\"220000\";s:11:\"number_page\";s:3:\"189\";s:12:\"publish_year\";s:4:\"1999\";s:14:\"publish_author\";s:11:\"Kim Đồng\";s:5:\"thumb\";s:13:\"images/p2.jpg\";s:11:\"category_id\";s:1:\"6\";s:6:\"number\";s:1:\"2\";}}', 1, '967000', 'complete'),
(11, 'a:3:{i:4;a:10:{s:2:\"id\";s:1:\"4\";s:4:\"name\";s:9:\"Quyển 4\";s:11:\"description\";s:225:\"“Hà Nội băm sáu phố phường” là tập bút kí nổi tiếng của nhà văn Thạch Lam, tập hợp lại những bài viết in trên báo sau khi ông qua đời, do Nhà xuất bản Văn học phát hành.\";s:5:\"price\";s:6:\"135000\";s:11:\"number_page\";s:3:\"343\";s:12:\"publish_year\";s:4:\"2000\";s:14:\"publish_author\";s:19:\"Vũ Trọng Phụng\";s:5:\"thumb\";s:13:\"images/p8.jpg\";s:11:\"category_id\";s:1:\"4\";s:6:\"number\";s:1:\"3\";}i:6;a:10:{s:2:\"id\";s:1:\"6\";s:4:\"name\";s:9:\"Quyển 6\";s:11:\"description\";s:282:\"Đây là phần chính của bài giới thiệu. Yêu cầu chung của phần này là phải (1) khái quát, tóm tắt được nội dung chủ đề tác phẩm, (2) nêu được các giá trị về nội dung của tác phẩm đối với xã hội và bạn đọc.\r\n\r\n\";s:5:\"price\";s:6:\"220000\";s:11:\"number_page\";s:3:\"189\";s:12:\"publish_year\";s:4:\"1999\";s:14:\"publish_author\";s:11:\"Kim Đồng\";s:5:\"thumb\";s:13:\"images/p2.jpg\";s:11:\"category_id\";s:1:\"6\";s:6:\"number\";s:1:\"3\";}i:5;a:10:{s:2:\"id\";s:1:\"5\";s:4:\"name\";s:9:\"Quyển 5\";s:11:\"description\";s:301:\"Sách không chỉ là nguồn tri thức vô tận đối với mỗi người mà ngày càng có nhiều minh chứng khoa học cho thấy những tác động rất tốt của việc đọc sách đối với sự phát triển cả về thể chất lẫn trí tuệ và tâm hồn của trẻ.\";s:5:\"price\";s:5:\"56000\";s:11:\"number_page\";s:3:\"158\";s:12:\"publish_year\";s:4:\"2004\";s:14:\"publish_author\";s:7:\"Nam Cao\";s:5:\"thumb\";s:13:\"images/p7.jpg\";s:11:\"category_id\";s:1:\"5\";s:6:\"number\";s:1:\"1\";}}', 1, '1121000', 'complete'),
(13, 'a:2:{i:6;a:10:{s:2:\"id\";s:1:\"6\";s:4:\"name\";s:9:\"Quyển 6\";s:11:\"description\";s:282:\"Đây là phần chính của bài giới thiệu. Yêu cầu chung của phần này là phải (1) khái quát, tóm tắt được nội dung chủ đề tác phẩm, (2) nêu được các giá trị về nội dung của tác phẩm đối với xã hội và bạn đọc.\r\n\r\n\";s:5:\"price\";s:6:\"220000\";s:11:\"number_page\";s:3:\"189\";s:12:\"publish_year\";s:4:\"1999\";s:14:\"publish_author\";s:11:\"Kim Đồng\";s:5:\"thumb\";s:13:\"images/p2.jpg\";s:11:\"category_id\";s:1:\"6\";s:6:\"number\";s:1:\"2\";}i:4;a:10:{s:2:\"id\";s:1:\"4\";s:4:\"name\";s:9:\"Quyển 4\";s:11:\"description\";s:225:\"“Hà Nội băm sáu phố phường” là tập bút kí nổi tiếng của nhà văn Thạch Lam, tập hợp lại những bài viết in trên báo sau khi ông qua đời, do Nhà xuất bản Văn học phát hành.\";s:5:\"price\";s:6:\"135000\";s:11:\"number_page\";s:3:\"343\";s:12:\"publish_year\";s:4:\"2000\";s:14:\"publish_author\";s:19:\"Vũ Trọng Phụng\";s:5:\"thumb\";s:13:\"images/p8.jpg\";s:11:\"category_id\";s:1:\"4\";s:6:\"number\";s:1:\"4\";}}', 3, '980000', 'complete'),
(14, 'a:2:{i:6;a:10:{s:2:\"id\";s:1:\"6\";s:4:\"name\";s:9:\"Quyển 6\";s:11:\"description\";s:282:\"Đây là phần chính của bài giới thiệu. Yêu cầu chung của phần này là phải (1) khái quát, tóm tắt được nội dung chủ đề tác phẩm, (2) nêu được các giá trị về nội dung của tác phẩm đối với xã hội và bạn đọc.\r\n\r\n\";s:5:\"price\";s:6:\"220000\";s:11:\"number_page\";s:3:\"189\";s:12:\"publish_year\";s:4:\"1999\";s:14:\"publish_author\";s:11:\"Kim Đồng\";s:5:\"thumb\";s:13:\"images/p2.jpg\";s:11:\"category_id\";s:1:\"6\";s:6:\"number\";s:1:\"2\";}i:4;a:10:{s:2:\"id\";s:1:\"4\";s:4:\"name\";s:9:\"Quyển 4\";s:11:\"description\";s:225:\"“Hà Nội băm sáu phố phường” là tập bút kí nổi tiếng của nhà văn Thạch Lam, tập hợp lại những bài viết in trên báo sau khi ông qua đời, do Nhà xuất bản Văn học phát hành.\";s:5:\"price\";s:6:\"135000\";s:11:\"number_page\";s:3:\"343\";s:12:\"publish_year\";s:4:\"2000\";s:14:\"publish_author\";s:19:\"Vũ Trọng Phụng\";s:5:\"thumb\";s:13:\"images/p8.jpg\";s:11:\"category_id\";s:1:\"4\";s:6:\"number\";s:1:\"4\";}}', 3, '980000', 'complete'),
(15, 'a:2:{i:6;a:10:{s:2:\"id\";s:1:\"6\";s:4:\"name\";s:9:\"Quyển 6\";s:11:\"description\";s:282:\"Đây là phần chính của bài giới thiệu. Yêu cầu chung của phần này là phải (1) khái quát, tóm tắt được nội dung chủ đề tác phẩm, (2) nêu được các giá trị về nội dung của tác phẩm đối với xã hội và bạn đọc.\r\n\r\n\";s:5:\"price\";s:6:\"220000\";s:11:\"number_page\";s:3:\"189\";s:12:\"publish_year\";s:4:\"1999\";s:14:\"publish_author\";s:11:\"Kim Đồng\";s:5:\"thumb\";s:13:\"images/p2.jpg\";s:11:\"category_id\";s:1:\"6\";s:6:\"number\";s:1:\"2\";}i:4;a:10:{s:2:\"id\";s:1:\"4\";s:4:\"name\";s:9:\"Quyển 4\";s:11:\"description\";s:225:\"“Hà Nội băm sáu phố phường” là tập bút kí nổi tiếng của nhà văn Thạch Lam, tập hợp lại những bài viết in trên báo sau khi ông qua đời, do Nhà xuất bản Văn học phát hành.\";s:5:\"price\";s:6:\"135000\";s:11:\"number_page\";s:3:\"343\";s:12:\"publish_year\";s:4:\"2000\";s:14:\"publish_author\";s:19:\"Vũ Trọng Phụng\";s:5:\"thumb\";s:13:\"images/p8.jpg\";s:11:\"category_id\";s:1:\"4\";s:6:\"number\";s:1:\"4\";}}', 3, '980000', 'complete'),
(16, 'a:2:{i:4;a:10:{s:2:\"id\";s:1:\"4\";s:4:\"name\";s:9:\"Quyển 4\";s:11:\"description\";s:225:\"“Hà Nội băm sáu phố phường” là tập bút kí nổi tiếng của nhà văn Thạch Lam, tập hợp lại những bài viết in trên báo sau khi ông qua đời, do Nhà xuất bản Văn học phát hành.\";s:5:\"price\";s:6:\"135000\";s:11:\"number_page\";s:3:\"343\";s:12:\"publish_year\";s:4:\"2000\";s:14:\"publish_author\";s:19:\"Vũ Trọng Phụng\";s:5:\"thumb\";s:13:\"images/p8.jpg\";s:11:\"category_id\";s:1:\"4\";s:6:\"number\";s:2:\"10\";}i:6;a:10:{s:2:\"id\";s:1:\"6\";s:4:\"name\";s:9:\"Quyển 6\";s:11:\"description\";s:282:\"Đây là phần chính của bài giới thiệu. Yêu cầu chung của phần này là phải (1) khái quát, tóm tắt được nội dung chủ đề tác phẩm, (2) nêu được các giá trị về nội dung của tác phẩm đối với xã hội và bạn đọc.\r\n\r\n\";s:5:\"price\";s:6:\"220000\";s:11:\"number_page\";s:3:\"189\";s:12:\"publish_year\";s:4:\"1999\";s:14:\"publish_author\";s:11:\"Kim Đồng\";s:5:\"thumb\";s:13:\"images/p2.jpg\";s:11:\"category_id\";s:1:\"6\";s:6:\"number\";s:1:\"1\";}}', 1, '1570000', 'complete'),
(17, 'a:3:{i:4;a:10:{s:2:\"id\";s:1:\"4\";s:4:\"name\";s:9:\"Quyển 4\";s:11:\"description\";s:225:\"“Hà Nội băm sáu phố phường” là tập bút kí nổi tiếng của nhà văn Thạch Lam, tập hợp lại những bài viết in trên báo sau khi ông qua đời, do Nhà xuất bản Văn học phát hành.\";s:5:\"price\";s:6:\"135000\";s:11:\"number_page\";s:3:\"343\";s:12:\"publish_year\";s:4:\"2000\";s:14:\"publish_author\";s:19:\"Vũ Trọng Phụng\";s:5:\"thumb\";s:13:\"images/p8.jpg\";s:11:\"category_id\";s:1:\"4\";s:6:\"number\";s:2:\"10\";}i:6;a:10:{s:2:\"id\";s:1:\"6\";s:4:\"name\";s:9:\"Quyển 6\";s:11:\"description\";s:282:\"Đây là phần chính của bài giới thiệu. Yêu cầu chung của phần này là phải (1) khái quát, tóm tắt được nội dung chủ đề tác phẩm, (2) nêu được các giá trị về nội dung của tác phẩm đối với xã hội và bạn đọc.\r\n\r\n\";s:5:\"price\";s:6:\"220000\";s:11:\"number_page\";s:3:\"189\";s:12:\"publish_year\";s:4:\"1999\";s:14:\"publish_author\";s:11:\"Kim Đồng\";s:5:\"thumb\";s:13:\"images/p2.jpg\";s:11:\"category_id\";s:1:\"6\";s:6:\"number\";s:1:\"1\";}i:7;a:10:{s:2:\"id\";s:1:\"7\";s:4:\"name\";s:9:\"Quyển 6\";s:11:\"description\";s:241:\"Có thể nói, trong lý luận cũng như thực tiễn biên mục mô tả Việt Nam đã có nhiều trăn trở để đi đến thống nhất trong quy tắc biên mục mô tả nói chung và phương pháp lập TĐMT nói riêng\";s:5:\"price\";s:6:\"108000\";s:11:\"number_page\";s:3:\"167\";s:12:\"publish_year\";s:4:\"1998\";s:14:\"publish_author\";s:11:\"Kim Đồng\";s:5:\"thumb\";s:13:\"images/p8.jpg\";s:11:\"category_id\";s:1:\"1\";s:6:\"number\";i:1;}}', 1, '1678000', 'complete');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(5, 'Sách giáo khoa'),
(6, 'Truyện ngắn'),
(8, 'Truyện cười');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu`
--

CREATE TABLE `menu` (
  `id` int(9) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` text COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menu`
--

INSERT INTO `menu` (`id`, `name`, `link`, `parent_id`) VALUES
(1, '<i class=\"fas fa-home\"></i> Trang chủ', 'http://localhost/sachjvb/', 0),
(2, '<i class=\"fas fa-book-reader\"></i> Sách', '#', 0),
(3, 'Giới thiệu', 'http://localhost/sachjvb/page/introduce', 0),
(4, '<i class=\"fa fa-phone\"></i> Liên hệ', 'http://localhost/sachjvb/page/contact', 0),
(5, 'Sách giáo khoa', 'http://localhost/sachjvb/category/detail/5', 2),
(6, 'Truyện ngắn', 'http://localhost/sachjvb/category/detail/6', 2),
(7, 'Truyện cười', 'http://localhost/sachjvb/category/detail/8', 2),
(9, 'Sách giáo khoarwerwerr', 'http://localhost/sachjvb/category/detail/5', 6);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `fullname`, `username`, `password`, `email`, `role`) VALUES
(1, 'Admin', 'admin', '202cb962ac59075b964b07152d234b70', 'jreamoq@gmail.com', 0),
(2, 'Linh', 'linh', '202cb962ac59075b964b07152d234b70', 'nguyenthilinh@gmail.com', 1),
(3, 'Van', 'van', '202cb962ac59075b964b07152d234b70', 'vuthivan@gmail.com', 2),
(4, 'Huy', 'huy', '202cb962ac59075b964b07152d234b70', 'nguyenvanhuy@gmail.com', 3),
(11, 'rr', 'qqqqqq', '202cb962ac59075b964b07152d234b70', 'tinviet.tk@gmail.com', 1),
(20, 'Lan', 'nguyenthilan', '81dc9bdb52d04dc20036dbd8313ed055', 'nguyenthilan@gmail.com', 1),
(21, 'Lan', 'nguyenthilan', '81dc9bdb52d04dc20036dbd8313ed055', 'nguyenthilan@gmail.com', 1),
(22, 'Lan', 'nguyenthilan', '81dc9bdb52d04dc20036dbd8313ed055', 'nguyenthilan@gmail.com', 1),
(24, 'Nguyễn Thị Nhung', '', 'e10adc3949ba59abbe56e057f20f883e', 'hoabinh.chim.52', 1);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `book_order`
--
ALTER TABLE `book_order`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `books`
--
ALTER TABLE `books`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `book_order`
--
ALTER TABLE `book_order`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
