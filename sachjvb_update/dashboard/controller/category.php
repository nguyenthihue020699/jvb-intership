<?php
class Category extends Base_controller{
	public function __construct(){
		parent:: __construct();
	}

	public function add_Category(){
		if(isset($_POST['add_category'])){
			/*echo(1233);die;*/
			$err = array();

			$name = UE_Input::post('name');
			if(empty($name)){
				array_push($err, 'Tên danh mục không được trống');
			}
			if(!empty($err)){
				$err_str = implode('<br />', $err);
				UE_Message::add($err_str, 'message', 'danger');
			}else{
				$post_data = $_POST;
				$res = $this->model->addCategory($post_data);
				if($res > 0){
				//$message =  "Thanh cong";
					UE_Message::add('Thêm mới thành công', 'message', 'success');
				}else{
					UE_Message::add('Thêm mới thất bại', 'message', 'danger');
				}
			}
		}
		$this->loadView('category/addCategory');
	}
	public function listCategory(){
		$data = $this->model->getCategory();
		$this->loadView('category/category_view', array('res' => $data));
	}
	public function getIdCategory(){
		$data = $this->model->getIDCategory();
		$this->loadView('book/addBook', array('res' => $data));
	}
	public function delete($category_id){
		if(!empty($category_id)){
			$res = $this->model->delete($category_id);
			if($res > 0){
				UE_Message::add('Bạn đã xóa thành công', 'category', 'success');
			}else{
				UE_Message::add('Đã có lỗi xảy ra', 'category', 'warning');
			}
		}
		header('location: ' . ue_get_admin_link('category', 'listCategory'));
	}
	/*public function join_books(){
		$this->loadView('')
	}*/
}
?>