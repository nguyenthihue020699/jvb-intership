<?php
class Book extends Base_Controller{
	public function __construct() {
		parent::__construct();
	}

	public function index(){
		$data = $this->model->getAllBooks();
		$this->loadView('book/index', array('data' => $data));
	}

	public function delete($book_id){
		if(!empty($book_id)){
			$res = $this->model->delete($book_id);
			if($res > 0){
				UE_Message::add('Bạn đã xóa thành công', 'book', 'success');
			}else{
				UE_Message::add('Đã có lỗi xảy ra', 'book', 'warning');
			}
		}
		header('location: ' . ue_get_admin_link('book', 'index'));
	}
	public function add_Books(){
		if(isset($_POST['add_book'])){
			$err = array();
			$name = UE_Input::post('name');
			if(empty($name)){
				array_push($err , 'Tên sách không được trống');
			}
			if(!empty($err)){
				$err_str = implode('<br />', $err);
				UE_Message::add($err_str, 'message', 'danger');
			}else{
				$post_data = $_POST;
				$res = $this->model->addBooks($post_data);
				if($res > 0){
					UE_Message::add('Thêm mới thành công', 'message', 'success');
				}else{
					UE_Message::add('Thêm mới thất bại', 'message', 'danger');
				}
			}
		}
		$this->loadModel('Category');
		$cate_data = $this->model->getCategory();
		$this->loadView('book/addBook', array('data' => $cate_data));
	}
}