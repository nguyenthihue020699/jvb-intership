<div class="table-book mt-4">
	<h3 class="mb-4">Danh mục <a href="<?php echo ue_get_admin_link('category','add_Category'); ?>" class="btn btn-info btn-sm float-right">Thêm mới</a></h3>
	<?php
	if(!empty($res)) {
		/*dd($res);die;*/
		UE_Message::show('book');
		?>
		<table class="table">
			<thead>
			<tr>
				<th scope="col">Tên danh mục</th>
				<th scope="col"></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($res as $k => $v){ ?>
			<tr>
				<td><?php echo $v['name']; ?></td>

				<td>
					<a href="" class="btn btn-info btn-sm">Sửa</a>
					<a href="<?php echo ue_get_admin_link('category', 'delete') . '/' . $v['id']; ?>" class="btn btn-danger btn-sm">Xóa</a>
				</td>
			</tr>
			<?php } ?>
			</tbody>
		</table>
		<?php
	}else{
		?>
		<div class="alert alert-warning">Không có quyển sách nào.</div>
		<?php
	}
	?>
</div>