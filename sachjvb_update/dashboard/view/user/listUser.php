<div class="table-book mt-4">
	<h3 class="mb-4">Nhân viên <a href="<?php echo ue_get_admin_link('userdb','add_User'); ?>" class="btn btn-info btn-sm float-right">Thêm mới</a></h3>
	<?php
	if(!empty($data)) {
		UE_Message::show('book');
		?>
		<table class="table">
			<thead>
			<tr>
				<th scope="col">Tên đầy đủ</th>
				<th scope="col">Tên đăng nhập</th>
				<th scope="col">Email</th>
				<th scope="col">Quyền</th>
				<th scope="col"></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($data as $k => $v){ ?>
			<tr>
				<td><?php echo $v['fullname']; ?></td>
				<td><?php echo $v['username']; ?></td>
				<td><?php echo $v['email']; ?></td>
				<td><?php echo $v['role']; ?></td>
				<td>
					<a href="" class="btn btn-info btn-sm">Sửa</a>
					<a href="<?php echo ue_get_admin_link('userdb', 'deleteUser') . '/' . $v['id']; ?>" class="btn btn-danger btn-sm">Xóa</a>
				</td>
			</tr>
			<?php } ?>
			</tbody>
		</table>
		<?php
	}else{
		?>
		<div class="alert alert-warning">Không có nhân viên nào.</div>
		<?php
	}
	?>
</div>