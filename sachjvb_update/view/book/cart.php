<?php
$res = UE_Input::get_session('cart');
if(!empty($res)){
?>
	<div class="list-product">
		<p><a href="#"><?php echo 'Danh sách <span>'. count($res) .'</span> sản phẩm'; ?></a></p>

	</div>
    <div style="overflow: hidden">
    <div style="width: 50%; float: left">
<?php
	echo '<form action="'. ue_get_link('cart', 'update') .'" method="POST">';
	echo '<div class="box cart-table"><table>';
	?>
    <tr>
        <th>STT</th>
        <th>Ảnh</th>
        <th>Sách</th>
        <th>Giá</th>
        <th>Số lượng</th>
        <th></th>
    </tr>
    <?php
    $i = 1;
	foreach ($res as $key => $value) {
	    $number = $value['number'];
	    echo '<tr>';
		$link_detail = ue_get_link('book', 'detail') . '&book_id=' . $value['id'];
		?>
        <td><?php echo $i; ?></td>
        <td><a href="<?php echo $link_detail; ?>"><img src="<?php echo SITEURL; ?>assets/<?php echo $value['thumb']; ?>" width="50" height="50"></a></td>
        <td><a href="<?php echo $link_detail; ?>"><?php echo $value['name']; ?></a></td>
        <td><?php echo ue_format_price($value['price']); ?></td>
        <td>
            <select name="number[]">
                <?php for ($j = 1; $j <= 100; $j++){
                    $select = '';
                    if($number == $j){
                        $select = 'selected';
                    }
                    ?>
                    <option value="<?php echo $j; ?>" <?php echo $select; ?>><?php echo $j; ?></option>
                <?php } ?>
            </select>
            <input type="hidden" name="book_id[]" value="<?php echo $value['id']; ?>" />
        </td>
        <td>
            <a href="<?php echo ue_get_link('cart', 'remove') . '&book_id=' . $value['id']; ?>" class="delete-book"><i class="fa fa-trash"></i></a>
        </td>
		<?php
		echo '</tr>';
		$i++;
	}
	echo '</table></div>';
	echo '<button type="submit" name="update_cart">Cập nhật</button>';
    echo '</form>';

	?>
        <div class="total-price">
            Tổng tiền:
		    <?php
		    $total_price = ue_get_cart_total_price();
		    echo ue_format_price($total_price);
		    ?>
            <form action="<?php echo ue_get_link('order', 'create_order'); ?>" method="POST">
                <button type="submit" name="create_order">Thanh toán</button>
            </form>
        </div>
    </div>

    </div>
    <?php
}else{
	echo 'Không có quyển sách nào trong giỏ hàng';
}
