<?php
//Xu ly nghiep vu
class Customer_Model extends Base_Model {
	public function __construct() {
		parent::__construct(); //Tham chieu truc tiep den doi tuong cua lop cha gan nhat
	}

	// Lay ra danh sach cac khach hang
	public function getAllCustomer(){
		$sql = 'SELECT * FROM khachhang';
		$query = mysqli_query($this->conn, $sql);
		$result = array();
		if($query->num_rows > 0){
			while ($row = mysqli_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}


	//Them du lieu vao bang khach hang
	public function addCustomer($data){
		$sql = "INSERT INTO khachhang (MA_KH, HO_TEN, DIA_CHI, SO_DT) VALUES ('{$data['makh']}', '{$data['hoten']}', '{$data['diachi']}', '{$data['sdt']}')";
		$query = mysqli_query($this->conn, $sql);
		return $query;
	}
}