<?php

class Base_Controller{
	public function __construct() {

	}

	//loadView (include)
	public function loadView($path, $data = array()){
		$path = 'view/' . $path . '.php';
		if(file_exists($path)){
			extract($data);
			include $path;
		}
	}
}