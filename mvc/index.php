<?php


class JVB{
	public function __construct() {
		$this->init();
	}

	public function init(){
		$core_files = array(
			'core/Base_Model',
			'core/Base_Controller',
		);

		$files = array(
			'model/customer',
			'controller/customer'
		);
		$this->load($core_files);
		$this->load($files);
		//dd($core_files);
	}

	private function load($arr){ //
		foreach ($arr as $k => $v){
			$path = $v . '.php';
			if(file_exists($path)){
				require_once $path;
				/*dd(123);*/
			}
		}
	}
}
/* Goi truc tiep toi function controller */
new JVB();
$customer = new Customer();
$customer->add();
$customer->index();


function dd($arr){
	echo '<pre>';
	print_r($arr);
	echo '</pre>';
}
