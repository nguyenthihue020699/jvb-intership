<?php

class Customer extends Base_Controller{

	//Xu ly su kien
	private $model;
	public function __construct() {
		parent::__construct();
		$this->model = new Customer_Model();

		/*dd(1234);*/
	}

	public function index(){
		$data = $this->model->getAllCustomer(); 

		//loadView files list.php
		$this->loadView('customer/list', array('res' => $data));
	}

	// Xu ly cho su kien add
	public function add(){
		//loadView files add.php
		$this->loadView('customer/add');
		if(isset($_POST['add_customer'])){
			$post_data = $_POST;
			$res = $this->model->addCustomer($post_data);
			if($res > 0){
				echo 'Thanh cong';
			}else{
				echo 'Khong thanh cong';
			}
		}
	}
}