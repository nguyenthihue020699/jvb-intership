<?php
if(!function_exists('dd')){
	function dd($arr){
		echo '<pre>';
		print_r($arr);
		echo '</pre>';
	}
}

if(!function_exists('ue_get_link')){
	function ue_get_link($controller = '', $action = 'index'){
		return SITEURL . $controller . '/' . $action;
	}
}


if(!function_exists('ue_is_login')){
	function ue_is_login(){
		$logged = UE_Input::get_session('login');
		if(!empty($logged)){
			return true;
		}else{
			return false;
		}
	}
}

if(!function_exists('ue_get_user_data')){
	function ue_get_user_data(){
		return UE_Input::get_session('login');
	}
}
