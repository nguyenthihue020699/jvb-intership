<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Form</title>
	<link rel="stylesheet" type="text/css" href="<?php echo SITEURL; ?>assets/css/form.css">
</head>
<body>
	<div class="header">
		<div class="header1">
			<div class="search">
				<ul>
					<?php 
					if(ue_is_login()){
					?>
					<li><a href="<?php echo ue_get_link('user', 'logout'); ?>">Đăng xuất</a></li>
					<?php
					}else{
						?>
						<li><a href="<?php echo ue_get_link('user', 'login'); ?>">Đăng nhập</a></li>
						<?php	
					}
					?>
					<li><a href="<?php echo ue_get_link('user', 'signup'); ?>">Đăng ký</a></li>
					<li><?php
    				if(ue_is_login()){
    				    $user_data = ue_get_user_data();
       					echo 'Xin chào, ' . $user_data['name'];
    				}
    				?></li>
				</ul>
			</div>
		</div>
	</div>