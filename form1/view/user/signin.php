<?php
if(ue_is_login()){
    header('location: ' . SITEURL);
    exit();
}
?>
<div class="login">
	<form method="post" action="">
		<div class="header-signin">
			<p>ĐĂNG NHẬP</p>
		</div>
		<div class="log">
			<input type="text" name="mail" placeholder="Tài khoản" >
			<input type="password" name="password" placeholder="Mật khẩu" >
			<?php
	            if(isset($errors)){
		            ?>
                    <div class="alert alert-danger small"><?php echo $errors; ?></div>
		            <?php
	            }
	            if(isset($message)){
		            ?>
                   	<div class="alert alert-success small"><?php echo $message; ?></div>
                   	
		            <?php
	            }
	        ?>
			<button name="login_action">Đăng nhập</button>
		</div>
	</form>
</div>