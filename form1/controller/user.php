<?php
class User extends Base_Controller{
	public function __construct() {
		parent::__construct();

	}
	public function index(){
		$this->loadView('user/body');
	}
	public function logout(){
		if(isset($_SESSION['login'])){
			unset($_SESSION['login']);
		}
		header('location: ' . SITEURL);
		exit();
	}

	public function login(){
		if(isset($_POST['login_action'])){
			$mail = UE_Input::post('mail');
			$password = UE_Input::post('password');
			if(empty($mail) || empty($password)){
				$this->loadView('user/signin', array('errors' => 'Tài khoản hoặc Mật khẩu không được trống.'));
			}else{
				$user_data = $this->model->getUserData($mail, $password);
				if(!empty($user_data)){
					$_SESSION['login'] = array_shift($user_data);
					$this->loadView('user/signin', array('message' => 'Đăng nhập thành công.'));
				}else{
					$this->loadView('user/signin', array('errors' => 'Đăng nhập thất bại.'));
				}
			}
			return;
		}
		$this->loadView('user/signin');
	}
	public function signup(){
		$val = new UE_Validate();

		if(isset($_POST['signup_action'])){
			$err = array();

			$name = UE_Input::post('name');
			$email = UE_Input::post('mail');
			$password = UE_Input::post('password');
			$re_password = UE_Input::post('re_password');
			$birthday  = UE_Input::post('birthday');
			$phone  = UE_Input::post('phone');

			if(empty($name)){
				array_push($err, 'Tên tài khoản không được trống');
			}else{
				if($val->minLength($name, 3)){
					array_push($err, 'Tên tài khoản phải lớn hơn hoặc bằng 3 ký tự');	
				}else if($val->maxLength($name, 30)){
					array_push($err, 'Tên tài khoản phải nhỏ hơn hoặc bằng 30 ký tự');
				}else{
					$check_username = $this->model->username_exists($name);
					if($check_username){
						array_push($err, 'Tên tài khoản đã tồn tại');	
					}
				}
			}

			if(empty($email)){
				array_push($err, 'Email không được trống');
			}else{
				if(!$val->isEmail($email)){
					array_push($err, 'Email không đúng định dạng');	
				}else{
					$check_email = $this->model->email_exists($email);
					if($check_email){
						array_push($err, 'Email đã tồn tại');	
					}
				}
			}

			if(empty($password)){
				array_push($err, 'Mật khẩu không được trống');
			}else{
				if($password != $re_password){
					array_push($err, 'Mật khẩu không không giống nhau');
				}else if($val->minLength($password, 8)){
					array_push($err, 'Mật khẩu phải lớn hơn hoặc bằng 8 ký tự');	
				}else if($val->maxLength($password, 32)){
					array_push($err, 'Mật khẩu phải nhỏ hơn hoặc bằng 32 ký tự');	
				}
			}

			if(!empty($err)){
				$err_str = implode('<br />', $err);
				UE_Message::add($err_str, 'message', 'danger');
			}else{
				$post_data = $_POST;
				$res = $this->model->add_user($post_data);
				if($res > 0){
					UE_Message::add('Đăng ký thành công', 'message', 'success');
				}else{
					UE_Message::add('Đăng ký thất bại', 'message', 'danger');
				}
			}
			
		}

		$this->loadView('user/signup');
	}
}