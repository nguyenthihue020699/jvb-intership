<?php
class UE_Message{
	public static function add($message = '', $key = 'message'){
		$_SESSION[$key] = $message;
	}

	public static function show($key = 'message'){
		if(isset($_SESSION[$key])){
			echo $_SESSION[$key];
		}
		unset($_SESSION[$key]);
	}

}