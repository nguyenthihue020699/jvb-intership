<?php
class Book_Model extends Base_Model{
	public function __construct(){
		parent:: __construct();
	}

	public function getAllBooks(){
		$sql = 'SELECT * FROM books order by id DESC';
		$query = mysqli_query($this->conn, $sql);
		$result = array();
		if(!empty($query) && $query->num_rows > 0){
			while ($row = mysqli_fetch_assoc($query)) {
				$result[] = $row;
			}
		}
		return $result;
	}

	public function getBooks($search_text){
		$sql = "SELECT * FROM books WHERE name LIKE '%{$search_text}%' OR description LIKE '%{$search_text}%'";
		//echo $sql;die;
		$query = mysqli_query($this->conn, $sql);
		//dd($query);die;
		$result = array();
		if(!empty($query) && $query->num_rows > 0){
			while ($row = mysqli_fetch_assoc($query)) {
				# code...
				$result[] = $row;
			}
		}
		return $result;
	}

	public function getBookByID($book_id){
		$sql = "SELECT * FROM books WHERE id = $book_id";
		$query = mysqli_query($this->conn, $sql);
		$result = array();
		if(!empty($query) && $query->num_rows > 0){
			while ($row = mysqli_fetch_assoc($query)) {
				$result[] = $row;
			}
		}
		return $result;
	}
}
?>