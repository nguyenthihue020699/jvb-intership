<?php

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Sách</title>
	<link href="<?php echo SITEURL; ?>assets/plugins/fontawesome/css/all.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo SITEURL; ?>assets/css/books.css">
	<link rel="stylesheet" type="text/css" href="<?php echo SITEURL; ?>assets/css/login.css">
</head>
<body>
	<div class="header">
		<div class="header1">
			<a href="<?php echo SITEURL; ?>"><img src="<?php echo SITEURL; ?>assets/images/logomaumoi.png" width="200" height="50"></a>
			<div class="search">
				<ul>
					<li class="get">
						<form method="GET" action="<?php echo ue_get_link('book', 'search'); ?>">
							<input class="search1" type="text" name="s" placeholder="Tìm kiếm" required="" value="<?php echo isset($_GET['s']) ? $_GET['s'] : ''; ?>">
							<input class="button" type="submit" value="Tìm">
						</form>
					</li>
					<li><a href="<?php echo ue_get_link('cart', 'detail') ?>"><i class="fas fa-shopping-cart"></i>
						<?php
						if(!empty(UE_Input::get_session('cart'))){
							echo '(' . count(UE_Input::get_session('cart')) . ')';
						}
						?>
					</a></li>
					<?php 
					if(ue_is_login()){
					?>
					<li><a href="<?php echo ue_get_link('user', 'logout'); ?>">Đăng xuất</a></li>
					<?php
					}else{
						?>
						<li><a href="<?php echo ue_get_link('user', 'login'); ?>">Đăng nhập</a></li>
						<?php	
					}
					?>
					<li><?php
    				if(ue_is_login()){
    				    $user_data = ue_get_user_data();
       					echo 'Xin chào, ' . $user_data['fullname'];
    				}
    				?></li>
				</ul>
			</div>
		</div>
		<div class="menu">
			<div class="container">
				<a href="<?php echo SITEURL; ?>">	<i class="fas fa-home"></i> Trang chủ</a>
				<a href="http://localhost/sach/mvc/?page=category"><i class="fas fa-book-reader"></i> Sách
					<ul>
						<li>Sách giáo khoa</li>
						<li>Tiểu thuyết</li>
						<li>Truyện ngắn</li>
						<li>Truyện cười</li>
					</ul>
				</a>
				<a href="<?php echo ue_get_link('page', 'introduce'); ?>">Giới thiệu</a>
				<a href="<?php echo ue_get_link('page', 'contact'); ?>"><i class="fa fa-phone" aria-hidden="true"></i> Liên hệ</a>
				<a href="http://localhost/Sach/mvc/?page=manager"><i class="fa fa-users" aria-hidden="true"></i> Nhân viên</a>
			</div>
		</div>
	</div>
	<div class="body">
		<div class="slider">
			<img class="item active slider-item " src="<?php echo SITEURL; ?>assets/images/Banner_1920-95__Hội_sách___3_.jpg" data-id="0">
			<img class="item slider-item " src="<?php echo SITEURL; ?>assets/images/header_Sinh-nhat.jpg" data-id="1">
		</div>
	</div>
<div class="content">
    <div class="container">