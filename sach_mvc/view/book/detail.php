<?php
if(!empty($res)){
	echo '<div class="card">';
	foreach ($res as $key => $value) {
		?>
		<div class="col-left">
			<img src="<?php echo SITEURL; ?>assets/<?php echo $value['thumb']; ?>" width="250" height="300" >
		</div>
		<div class="col-right">
			<h3 class="title-product"><?php echo $value['name']; ?></h3>
			<p class="product-des">
				<?php echo $value['description']; ?>
			</p>
			<h4><p>Giá bán: <?php echo ue_format_price($value['price']); ?></p></h4>
			<p>Số trang: <?php echo $value['number_page']; ?> </p>
			<p>Nhà xuất bản: <?php echo $value['publish_author']; ?></p>
			<p>Năm xuất bản: <?php echo $value['publish_year']; ?></p>
			<form action="<?php echo ue_get_link('cart', 'add_cart'); ?>" method="POST">
				<input type="hidden" name="book_id" value="<?php echo $value['id']; ?>">
				<button  class="buy-book" name="add_to_cart" value="1">Mua sách</button>
			</form>
		</div>
		</div>
		<?php
	}
	echo '</div>';
}else{
	echo 'Không tồn tại quyển sách này.';
}
