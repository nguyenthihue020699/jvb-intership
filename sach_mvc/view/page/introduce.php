<?php

?>
<div class=" introduce">
	<h3>GIỚI THIỆU</h3>
	<p>Đến với hiệu sách nhanvan.vn bạn như được trải nghiệm vào một thế giới sách rộng lớn, gồm nhiều tập truyện hay đầy đủ, chi tiết và cập nhật nhiều truyện mới nhất. Ở đây cũng là một nơi có rất nhiều sách giáo khoa dành cho học sinh từ lớp 1 đến lớp 12 ngoài ra còn có nhiều sách bài tập, nâng cao để học sinh có thể rèn luyện được tốt hơn. Nếu bạn muốn tìm đọc những câu truyện của những mảnh đời bất hạnh, những câu truyện đời thường thực tế của những năm chiến tranh đau khổ thì mời bạn tìm đến những câu truyện ngắn của Kim Lân, Nam Cao,.... 
	</br>
	Nếu bạn muốn trải nghiệm với những truyện cười đầy thú vị mời bạn tìm đến đọc mục truyện cười...</p>
</div>
<div class="content">
	<div class="recruitment-left">
		<div class="recruitment">
			<div class="col-left">
				<a href="#"><img  src="<?php echo SITEURL; ?>assets/images/sgk.jpg" width="100" height="100"></a>
			</div>
			<div class="col-right">
				<div class="title">SÁCH GIÁO KHOA</div>
				<div class="introduce">Tổng hợp đầy đủ sách giáo khoa từ lớp 1 đến lớp 12</div>
				<div class="Salary"><a href="#">Xem chi tiết</a></div>
			</div>
		</div>

		<div class="recruitment">
			<div class="col-left">
				<a href="#"><img  src="<?php echo SITEURL; ?>assets/images/tt.jpg" width="100" height="100"></a>
			</div>
			<div class="col-right">
				<div class="title">TIỂU THUYẾT</div>
				<div class="introduce">Tổng hợp đầy đủ sách giáo khoa từ lớp 1 đến lớp 12</div>
				<div class="Salary"><a href="#">Xem chi tiết</a></div>
			</div>
		</div>

		<div class="recruitment">
			<div class="col-left">
				<a href="#"><img  src="<?php echo SITEURL; ?>assets/images/tn.jpg" width="100" height="100"></a>
			</div>
			<div class="col-right">
				<div class="title">TRUYỆN NGẮN</div>
				<div class="introduce">Tổng hợp đầy đủ sách giáo khoa từ lớp 1 đến lớp 12 </div>
				<div class="Salary"><a href="#">Xem chi tiết</a></div>
			</div>
		</div>

		<div class="recruitment">
			<div class="col-left">
				<a href="#"><img  src="<?php echo SITEURL; ?>assets/images/tc.jpg" width="100" height="100"></a>
			</div>
			<div class="col-right">
				<div class="title">TRUYỆN CƯỜI</div>
				<div class="introduce">Tổng hợp đầy đủ sách giáo khoa từ lớp 1 đến lớp 12</div>
				<div class="Salary"><a href="#">Xem chi tiết</a></div>
			</div>
		</div>
	</div>
	<div class="recruitment-right">
		<a href="#"><img src="<?php echo SITEURL; ?>assets/images/book.jpg" /></a>
		<div class="list">
		<h3>SÁCH HAY TRONG TUẦN</h3>
			<div class="list1">
			<ul>
				<li><a href="#">Đời thừa - Nam Cao</a></li>
				<li><a href="#">Vợ nhặt - Kim Lân</a></li>
				<li><a href="#">Nắng nghịch mùa</a></li>
				<li><a href="#">Giấc mơ trở về</a></li>
				<li><a href="#">Chờ một ngày nắng</a></li>
				<li><a href="#">Chớp bể mưa nguồn</a></li>
				<li><a href="#">Tơ vò</a></li>
			</ul>
			</div>
		</div>
	</div>
</div>