<?php
if(!function_exists('ue_cut_string')){
	function ue_cut_string($str){
		$count_str = 75;
		if($count_str >= strlen($str)){
			return $str;
		}else{
			$str = substr($str, 0, $count_str);
			return $str . '...';
		}		
	}
}