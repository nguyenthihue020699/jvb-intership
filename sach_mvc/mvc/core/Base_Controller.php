<?php
class Base_Controller{
	public function __construct(){

	}

	public function loadView($path, $data = array()){
		/*echo $path;die;*/
		$path = 'view/'.$path .'.php';
		
		if(file_exists($path)){
			/*echo $path; die;*/
			extract($data);
			include "view/header.php";
			include $path;
			include "view/footer.php";
		}
	}
}
