<?php

class Customer_Model extends Base_Model{
	public function __construct(){
		parent:: __construct();
	}

	public function getAllBooks(){
		$sql = 'SELECT * FROM book order by id DESC';
		$query = mysqli_query($this->conn, $sql);
		$result = array();
		if($query->num_rows > 0){
			while ($row = mysqli_fetch_assoc($query)) {
				$result[] = $row;
			}
		}
		return $result;
	}

	public function addBooks($data){
		
		/*dd(1234); die;*/
		$sql = "INSERT INTO book (image, name, price) VALUES ('{$data['image']}', '{$data['name']}', '{$data['price']}')";
		$query = mysqli_query($this->conn, $sql);
		return $query;
	}

	public function getAllCustomer(){
		/*dd(123565);*/
		$sql = 'SELECT * FROM khachhang';
		$query = mysqli_query($this->conn, $sql);
		$result = array();
		if($query->num_rows > 0){
			while ($row = mysqli_fetch_assoc($query)) {
				# code...
				$result[] = $row;
			}
		}
		return $result;
	}

	public function addCustomer($data){
		/*dd(1234); die;*/
		$sql = "INSERT INTO khachhang (MA_KH, HO_TEN, DIA_CHI, SO_DT) VALUES ('{$data['makh']}', '{$data['hoten']}', '{$data['diachi']}', '{$data['sdt']}')";
		$query = mysqli_query($this->conn, $sql);
		return $query;
	}

	public function bookDataByID($book_id){
		/*dd(23333);die;*/
		$sql = "SELECT * FROM book WHERE id = $book_id";
		/*dd($sql);die;*/
		$query = mysqli_query($this->conn, $sql);

		$result = array();
		if(!empty($query)) {
            if ($query->num_rows > 0) {
                while ($row = mysqli_fetch_assoc($query)) {
                    # code...
                    $result[] = $row;
                }
            }
        }
		return $result;
	}
	public function getLevel($level){
		$sql = "SELECT * FROM nhanvien WHERE level = $level";
		$query = mysqli_query($this->conn, $sql);
		$result = array();
		if($query->num_rows > 0){
			while ($row = mysqli_fetch_assoc($query)) {
				# code...
				$result[] = $row;
			}
		}
		return $result;
	}
	public function delete($level){
		$sql = "DELETE FROM nhanvien WHERE level = $level";
		$query = mysqli_query($this->conn, $sql);
		/*dd($query);die;*/
		return $query;
	}
	public function getAllManager(){
		/*dd(123565);*/
		$sql = 'SELECT MA_NV, HO_TEN, level FROM nhanvien';
		$query = mysqli_query($this->conn, $sql);
		/*dd($query);die;*/
		$result = array();
		if($query->num_rows > 0){
			while ($row = mysqli_fetch_assoc($query)) {
				# code...
				$result[] = $row;
			}
		}
		return $result;
	}
	public function getListManager(){
		/*dd(123565);*/
		$sql = 'SELECT * FROM nhanvien';
		$query = mysqli_query($this->conn, $sql);
		/*dd($query);die;*/
		$result = array();
		if($query->num_rows > 0){
			while ($row = mysqli_fetch_assoc($query)) {
				# code...
				$result[] = $row;
			}
		}
		return $result;
	}
}