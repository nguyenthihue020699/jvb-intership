<?php

session_start();
class JVB{
	public function __construct() {
		$this->init();
	}

	public function init(){
		$helper_files = array(
			'helpers/helper'
		);

		$core_files = array(
			'core/Base_Model',
			'core/Base_Controller',
		);

		$files = array(
			'model/customer',
			'model/user',
			'model/book',
			'controller/customer',
			'controller/user',
			'controller/book',
			'controller/introduce'
		);
		$this->load($helper_files);
		$this->load($core_files);
		$this->load($files);
	}

	private function load($arr){
		foreach ($arr as $k => $v){
			$path = $v . '.php';
			if(file_exists($path)){
				require_once $path;
			}
		}
	}
}
/* Goi truc tiep toi function controller */
new JVB();
if(isset($_GET['page'])){
	switch ($_GET['page']){
		case 'add':
		/*dd(3234343);*/
			$customer = new Customer_Controller();
			$customer->add();
			break;
		case 'login':
			$user = new User();
			$user->login();
			break;
		case 'logout':
			$user = new User();
			$user->logout();
			break;
		case 'addbook':
			# code...
			$customer = new Customer_Controller();
			$customer->addbook();
			break;
		case 'addCart':
			$customer = new Customer_Controller();
			$customer->addToCart();
			break;
		case 'removeCart':
			$customer = new Customer_Controller();
			$customer->removeCart();
			break;
		case 'viewCart':
			$customer = new Customer_Controller();
			$customer->viewCart();
			break;
		case 'list':
			$customer = new Customer_Controller();
			$customer->listcustomer();
			break;
		case 'category':
			$customer = new Customer_Controller();
			$customer->category();
			/*dd(23454545);die;*/
			break;
		case 'manager':
			$customer = new Customer_Controller();
			$customer->deleteManager();
			break;
		case 'addManager':
			$customer = new Customer_Controller();
			$customer->addManager();
			break;
		case 'search':
			$book = new Book_Controller();
			$book->search();
			break;
		case 'detail':
			$customer = new Customer_Controller();
			$customer->detail();
			break;
		case 'introduce':
			$introduce = new Introduce();
			$introduce-> introduce();
			break;
		default:

			$customer = new Customer_Controller();
			$customer->index();
			break;
	}
}else{
	$customer = new Customer_Controller();
	$customer->index();
}
//$customer->add();


function dd($arr){
	echo '<pre>';
	print_r($arr);
	echo '</pre>';
}
