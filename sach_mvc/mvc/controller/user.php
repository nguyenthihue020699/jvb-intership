<?php

class User extends Base_Controller{
	private $model;
	public function __construct() {
		parent::__construct();
		$this->model = new User_Model();
	}

	public function login(){
		if(isset($_POST['login_action'])){
			$post_data = $_POST;
			if(empty($post_data['username']) || empty($post_data['password'])){
				$this->loadView('user/login', array('errors' => 'Tài khoản hoặc Mật khẩu không được trống.'));
			}else{
				/*echo "fdgfgf";die;*/
				$user_data = $this->model->getUserData($post_data['username'], $post_data['password']);
				if(!empty($user_data)){
					$_SESSION['login'] = array_shift($user_data);

					if(isset($_SESSION['cart_temp' . $_SESSION['login']['username']]) && !empty($_SESSION['cart_temp' . $_SESSION['login']['username']])){
						$_SESSION['cart'] = $_SESSION['cart_temp' . $_SESSION['login']['username']];
					}

					$this->loadView('user/login', array('message' => 'Đăng nhập thành công.'));
				}else{
					$this->loadView('user/login', array('errors' => 'Đăng nhập thất bại.'));
				}
			}
			return;
		}
		$this->loadView('user/login');
	}

	public function logout(){
		if(isset($_SESSION['login'])){
			$_SESSION['cart_temp' . $_SESSION['login']['username']] = $_SESSION['cart'];

			unset($_SESSION['cart']);
			unset($_SESSION['login']);
			/*$_SESSION['cart'] = '';*/
		}
		header('location: http://localhost/sach/mvc/');
		exit();
	}
}