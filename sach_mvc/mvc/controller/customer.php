<?php
class Customer_Controller extends Base_Controller{
	private $model;
	public function __construct(){
		parent:: __construct();
		$this->model = new Customer_Model();
	}
	/*public function getManager(){
		$data = $this->model->getAllManager();
		$this->loadView('customer/delete', array('res' => $data));
	}*/
	public function viewCart(){
		$this->loadView('customer/view_cart');
	}
	public function category(){
		$this->loadView('customer/category');
	}
	public function index(){
		/*dd(125678);die;*/
		//$data = $this->model->getAllCustomer();
		$data = $this->model->getAllBooks();
		$this->loadView('customer/list1', array('res' => $data));
	}

	public function listcustomer(){
		$data = $this->model->getAllCustomer();
		$this->loadView('customer/list' ,array('res' => $data));
	}

	public function add(){
		$this->loadView('customer/add');
		/*dd(23444);die;*/
		if(isset($_POST['add_customer'])){
			/*dd(1455);die;*/
			$post_data = $_POST;
			$res = $this->model->addCustomer($post_data);
			if($res > 0){
				echo "Thanh cong";
			}else{
				echo "Khong thanh cong";
			}
		}
	}
	public function addManager(){
		if(isset($_SESSION['login']) && (($_SESSION['login']['level']) == 1)){
			$this->loadView('addManager/add');
			/*dd(23444);die;*/
			if(isset($_POST['add_customer'])){
				/*dd(1455);die;*/
				$post_data = $_POST;
				$res = $this->model->getListManager($post_data);
				if($res > 0){
					echo "Thanh cong";
				}else{
					echo "Khong thanh cong";
				}
			}
		}
	}

	public function addbook(){
		if(isset($_SESSION['login']) && ((($_SESSION['login']['level']) == 1) || (($_SESSION['login']['level']) == 2))){
		/*dd(23444);die;*/
			$message = '';
			if(isset($_POST['add_book'])){
			/*dd(1455);die;*/
				$post_data = $_POST;
				$res = $this->model->addBooks($post_data);
				if($res > 0){
				//$message =  "Thanh cong";
					header('location: http://localhost/sach/mvc/');
					exit();
				}else{
					$message = "Khong thanh cong";
				}
			}
			$this->loadView('customer/addBook', array('message' => $message));
		}else{
			header('location: http://localhost/sach/mvc/');
		}
	}

	public function addToCart(){
		if(isset($_SESSION['login'])){
			if(isset($_POST['add_to_cart'])){
				/*dd($_POST['add_to_cart']);die;*/
				$post_data = $_POST;
				$book_id = $_POST['book_id'];
				$book_data = $this->model->bookDataByID($post_data['book_id']);
				if(!empty($book_data)){
					$book_data = array_shift($book_data);
					/*dd($book_data);die;*/
					$_SESSION['cart'][$book_data['id']] = $book_data;
				}
			}
			header('location: http://localhost/sach/mvc/');
		}else {
			header('location: http://localhost/sach/mvc/');
		}
		
	}
	public function detail(){
		$dt = '';
		if(isset($_GET['dt'])){
			$dt = $_GET['dt'];
		}
		if(!empty($dt)) {
            $data = $this->model->bookDataByID($dt);
        }else{
		    $data = array();
        }
		$this->loadView('customer/detail', array('res' => $data));
	}
	public function removeCart(){
		if(isset($_SESSION['login'])){
			if(isset($_POST['remove_cart'])){
				$post_data = $_POST;
				$book_id = $_POST['book_id'];
				/*dd($_SESSION['cart']);die;*/
				if(!empty($post_data)){
					if(isset($_SESSION['cart']) && isset($_SESSION['cart'][$book_id])){
						unset($_SESSION['cart'][$book_id]);//remove theo id của cart (theo id của sách)
					}
				}
			}
			header('location: http://localhost/sach/mvc/');
		}
		
	}
	public function deleteManager(){
		/*dd(13444);die;*/
		$data = $this->model->getAllManager();
		$this->loadView('customer/delete', array('res' => $data));
		if(isset($_SESSION['login']) && ($_SESSION['login']['level']) == 1){
			/*dd(($_SESSION['login']['level']) + 1);die;*/
			/*dd(12344);die;*/
			/*dd($_POST['deleteManager']); die;*/
			if(isset($_POST['deleteManager'])){
				$post_data = $_POST;
				$post_level = $_POST['level'];
				/*dd($post_level);die;*/
				/*($_SESSION['login']['level']) + 1;*/
			/*	dd($post_level);die;*/
				if(!empty($post_data)){
					$this->model->delete($post_level);
				}
			}
		}
	}

	/*public function delete(){
		if(isset($_SESSION['login']) && empty(var))
	}*/

}