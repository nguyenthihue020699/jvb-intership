<?php
class Book_Controller extends Base_controller{
	private $model;
	public function __construct(){
		parent:: __construct();
		$this->model = new Book_Model();
	}

	public function search(){
		$s = '';
		if(isset($_GET['s'])){
			$s = $_GET['s'];
		}

		$data = $this->model->getBooks($s);
	
		$this->loadView('book/result', array('res' => $data));
	}
}
?>