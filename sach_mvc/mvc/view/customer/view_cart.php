<?php
if(!isset($_SESSION['cart']) && empty($_SESSION['cart'])){	
	return;
}
$res = $_SESSION['cart'];
?>
<div class="list-product">
	<p><a href="#"><?php echo 'Danh sách <span>'. count($res) .'</span> sản phẩm'; ?></a></p>

</div>
<?php
if(!empty($res)){
	echo '<div class="box">';
	foreach ($res as $key => $value) {
		?>
		<div class="item">
			<div class="section1">
				<a href="http://localhost/Sach/detail.html"><img src="assets/<?php echo $value['image']; ?>" width="247.5"></a>
			</div>
			<div class="section2">
				<p><a href="http://localhost/Sach/detail.html"><?php echo $value['name']; ?></a></p>
				<p class="price">Giá: <?php echo $value['price']; ?>đ</p>
				<p><form action="http://localhost/sach/mvc/?page=removeCart" method="POST">
					<input type="hidden" name="book_id" value="<?php echo$value['id']; ?>">
					<button class="delete-book" name="remove_cart">Xóa</button>	
				</form></p>
			</div>
		</div>
		<?php
	}
	echo '</div>';
}else{
	echo 'No Book';
}
