<?php

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Sách</title>
	<link href="assets/plugins/fontawesome/css/all.css" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="assets/css/books.css">
	<link rel="stylesheet" type="text/css" href="assets/css/login.css">
</head>
<body>
	<div class="header">
		<div class="header1">
			<a href="http://localhost/sach/mvc/"><img src="assets/images/logomaumoi.png" width="200" height="50"></a>
			<div class="search">
				<ul>
					<li class="get">
						<form method="GET" action="http://localhost/sach/mvc/">
							<input type="hidden" name="page" value="search">
							<input class="search1" type="text" name="s" placeholder="Tìm kiếm" required="" value="<?php echo isset($_GET['s']) ? $_GET['s'] : ''; ?>">
							<input class="button" type="submit" value="Tìm">
						</form>
					</li>
					<li><a href="http://localhost/sach/mvc/?page=viewCart"><i class="fas fa-shopping-cart"></i>
						<?php
						if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])){	
							echo '(' . count($_SESSION['cart']) . ' )';
						}
						?>
					</a></li>
					<?php 
					if(isset($_SESSION['login']) && !empty($_SESSION['login'])){
					?>
					<li><a href="http://localhost/sach/mvc/?page=logout">Đăng xuất</a></li>
					<?php
					}else{
						?>
						<li><a href="http://localhost/Sach/mvc/?page=login">Đăng nhập</a></li>
						<?php	
					}
					?>
					<li><?php
    				if(isset($_SESSION['login']) && !empty($_SESSION['login'])){
       					echo 'Xin chào, ' . $_SESSION['login']['HO_TEN'];
    				}
    				?></li>
				</ul>
			</div>
		</div>
		<div class="menu">
			<div class="container">
				<a href="http://localhost/sach/mvc/">	<i class="fas fa-home"></i> Trang chủ</a>
				<a href="http://localhost/sach/mvc/?page=category"><i class="fas fa-book-reader"></i> Sách
					<ul>
						<li>Sách giáo khoa</li>
						<li>Tiểu thuyết</li>
						<li>Truyện ngắn</li>
						<li>Truyện cười</li>
					</ul>
				</a>
				<a href="#">Giới thiệu</a>
				<!-- <a href="#">Kho sách cũ</a>
				<a href="#">Sách ngoại văn
					<ul>
						<li>ELT</li>
						<li>ELT</li>
					</ul>
				</a>
				<a href="#">Văn phòng phẩm
					<ul>
						<li>Máy tính</li>
						<li>Vở</li>
						<li>Bút</li>
					</ul>
				</a>
				<a href="#">Bách hóa online</a>
				<a href="#">Quà tặng</a> -->
			</div>
		</div>
	</div>
	<div class="body">
		<div class="slider">
			<img class="item active slider-item " src="assets/images/Banner_1920-95__Hội_sách___3_.jpg" data-id="0">
			<img class="item slider-item " src="assets/images/header_Sinh-nhat.jpg" data-id="1">
		</div>
	</div>
<div class="content">
    <div class="container">