<?php
class Page extends Base_Controller{
	public function __construct(){
		parent:: __construct();
	}
	public function introduce(){
		$this->loadView('page/introduce');
	}
	public function contact(){
		$this->loadView('page/contact');
	}
}
?>

