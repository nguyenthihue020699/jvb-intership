<?php
class book extends Base_controller{
	public function __construct(){
		parent:: __construct();
	}

	public function index(){
		$data = $this->model->getAllBooks();
		$this->loadView('book/list', array('res' => $data));
	}

	public function search(){
		$s = UE_Input::get('s');
		$data = $this->model->getBooks($s);
		$this->loadView('book/list', array('res' => $data));
	}

	public function detail($book_id = ''){
		if(!empty($book_id)) {
			$data = $this->model->getBookByID($book_id);
		}else{
			$data = array();
		}
		$this->loadView('book/detail', array('res' => $data));
	}
}
?>