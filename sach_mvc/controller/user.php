<?php
class User extends Base_Controller{
	public function __construct() {
		parent::__construct();
	}

	public function login(){
		if(isset($_POST['login_action'])){
			$username = UE_Input::post('username');
			$password = UE_Input::post('password');
			if(empty($username) || empty($password)){
				$this->loadView('user/login', array('errors' => 'Tài khoản hoặc Mật khẩu không được trống.'));
			}else{
				$user_data = $this->model->getUserData($username, $password);
				if(!empty($user_data)){
					$_SESSION['login'] = array_shift($user_data);

					if(isset($_SESSION['cart_temp' . $_SESSION['login']['username']]) && !empty($_SESSION['cart_temp' . $_SESSION['login']['username']])){
						$_SESSION['cart'] = $_SESSION['cart_temp' . $_SESSION['login']['username']];
					}
					$this->loadView('user/login', array('message' => 'Đăng nhập thành công.'));
				}else{
					$this->loadView('user/login', array('errors' => 'Đăng nhập thất bại.'));
				}
			}
			return;
		}
		$this->loadView('user/login');
	}

	public function logout(){
		if(isset($_SESSION['login'])){
			$_SESSION['cart_temp' . $_SESSION['login']['username']] = $_SESSION['cart'];
			unset($_SESSION['cart']);
			unset($_SESSION['login']);
		}
		header('location: ' . SITEURL);
		exit();
	}
}