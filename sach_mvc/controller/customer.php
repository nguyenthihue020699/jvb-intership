<?php
class Customer_Controller extends Base_Controller{
	private $model;
	public function __construct(){
		parent:: __construct();
		$this->model = new Customer_Model();
	}

	public function category(){
		$this->loadView('customer/category');
	}
	public function listcustomer(){
		$data = $this->model->getAllCustomer();
		$this->loadView('customer/list' ,array('res' => $data));
	}

	public function add(){
		$this->loadView('customer/add');
		/*dd(23444);die;*/
		if(isset($_POST['add_customer'])){
			/*dd(1455);die;*/
			$post_data = $_POST;
			$res = $this->model->addCustomer($post_data);
			if($res > 0){
				echo "Thanh cong";
			}else{
				echo "Khong thanh cong";
			}
		}
	}
	public function addManager(){
		if(isset($_SESSION['login']) && (($_SESSION['login']['level']) == 1)){
			$this->loadView('addManager/add');
			/*dd(23444);die;*/
			if(isset($_POST['add_customer'])){
				/*dd(1455);die;*/
				$post_data = $_POST;
				$res = $this->model->getListManager($post_data);
				if($res > 0){
					echo "Thanh cong";
				}else{
					echo "Khong thanh cong";
				}
			}
		}
	}

	public function addbook(){
		if(isset($_SESSION['login']) && ((($_SESSION['login']['level']) == 1) || (($_SESSION['login']['level']) == 2))){
		/*dd(23444);die;*/
			$message = '';
			if(isset($_POST['add_book'])){
			/*dd(1455);die;*/
				$post_data = $_POST;
				$res = $this->model->addBooks($post_data);
				if($res > 0){
				//$message =  "Thanh cong";
					header('location: http://localhost/sach/mvc/');
					exit();
				}else{
					$message = "Khong thanh cong";
				}
			}
			$this->loadView('customer/addBook', array('message' => $message));
		}else{
			header('location: http://localhost/sach/mvc/');
		}
	}

	public function deleteManager(){
		/*dd(13444);die;*/
		$data = $this->model->getAllManager();
		$this->loadView('customer/delete', array('res' => $data));
		if(isset($_SESSION['login']) && ($_SESSION['login']['level']) == 1){
			/*dd(($_SESSION['login']['level']) + 1);die;*/
			/*dd(12344);die;*/
			/*dd($_POST['deleteManager']); die;*/
			if(isset($_POST['deleteManager'])){
				$post_data = $_POST;
				$post_level = $_POST['level'];
				/*dd($post_level);die;*/
				/*($_SESSION['login']['level']) + 1;*/
			/*	dd($post_level);die;*/
				if(!empty($post_data)){
					$this->model->delete($post_level);
				}
			}
		}
	}

}