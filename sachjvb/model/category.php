
<?php
class Category_Model extends Base_Model{
	public function __construct(){
		parent:: __construct();
	}

	public function getCategoryByID($cate_id){
		$sql = "SELECT books.*, categories.id as cate_id, categories.name as cate_name FROM books INNER JOIN categories ON books.category_id = categories.id WHERE books.category_id = {$cate_id}";

		$query = mysqli_query($this->conn, $sql);	
		$result = array();
		if(!empty($query) && $query->num_rows > 0){
			while ($row = mysqli_fetch_assoc($query)) {
				$result[] = $row;
			}
		}
		return $result;
	}

	public function getCategory(){
		$sql = 'SELECT * FROM categories';
		$query = mysqli_query($this->conn, $sql);
		$result = array();
		if($query->num_rows > 0){
			while ($row = mysqli_fetch_assoc($query)) {
				# code...
				$result[] = $row;
			}
		}
		return $result;
	}
	public function addCategory($data){
		$sql = "INSERT INTO categories(name) VALUES('{$data['name']}')";
		$query = mysqli_query($this->conn, $sql);
		return $query;
	}
	public function delete($category_id){
		$sql = "DELETE FROM categories WHERE id = $category_id";
		$query = mysqli_query($this->conn, $sql);
		return $query;
	}


}
?>