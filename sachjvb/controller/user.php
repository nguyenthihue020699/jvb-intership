<?php
class User extends Base_Controller{
	public function __construct() {
		parent::__construct();

	}

	public function login(){
		if(isset($_POST['login_action'])){
			$username = UE_Input::post('username');
			$password = UE_Input::post('password');
			if(empty($username) || empty($password)){
				$this->loadView('user/login', array('errors' => 'Tài khoản hoặc Mật khẩu không được trống.'));
			}else{
				$user_data = $this->model->getUserData($username, $password);
				if(!empty($user_data)){
					$_SESSION['login'] = array_shift($user_data);

					if(isset($_SESSION['cart_temp' . $_SESSION['login']['username']]) && !empty($_SESSION['cart_temp' . $_SESSION['login']['username']])){
						$_SESSION['cart'] = $_SESSION['cart_temp' . $_SESSION['login']['username']];
					}
					$this->loadView('user/login', array('message' => 'Đăng nhập thành công.'));
				}else{
					$this->loadView('user/login', array('errors' => 'Đăng nhập thất bại.'));
				}
			}
			return;
		}
		$this->loadView('user/login');
	}
	/*public function signup(){
		if(isset($_POST['signup_action'])){
			$post_data = $_POST;
			$res = $this->model->add_user($post_data);
		}
		$this->loadView('user/signup');
	}*/

	public function logout(){
		if(isset($_SESSION['login'])){
			unset($_SESSION['login']);
		}
		header('location: ' . SITEURL);
		exit();
	}
	public function signup(){
		$val = new UE_Validate();
		//name
		//username
		//email
		//password
		//re_password

		if(isset($_POST['signup_action'])){
			$err = array();

			$name = UE_Input::post('name');
			$username = UE_Input::post('username');
			$email = UE_Input::post('email');
			$password = UE_Input::post('password');
			$re_password = UE_Input::post('re_password');

			if(empty($name)){
				array_push($err, 'Tên không được trống');
			}else{
				if($val->minLength($name, 2)){
					array_push($err, 'Tên phải lớn hơn 2 ký tự');	
				}
			}

			if(empty($username)){
				array_push($err, 'Tên tài khoản không được trống');
			}else{
				if($val->minLength($username, 6)){
					array_push($err, 'Tên tài khoản phải lớn hơn hoặc bằng 6 ký tự');	
				}else{
					$check_username = $this->model->username_exists($username);
					if($check_username){
						array_push($err, 'Tên tài khoản đã tồn tại');	
					}
				}
			}

			if(empty($email)){
				array_push($err, 'Email không được trống');
			}else{
				if(!$val->isEmail($email)){
					array_push($err, 'Email không đúng định dạng');	
				}else{
					$check_email = $this->model->email_exists($email);
					if($check_email){
						array_push($err, 'Email đã tồn tại');	
					}
				}
			}

			if(empty($password)){
				array_push($err, 'Mật khẩu không được trống');
			}else{
				if($password != $re_password){
					array_push($err, 'Mật khẩu không không giống nhau');
				}
			}

			if(!empty($err)){
				$err_str = implode('<br />', $err);
				UE_Message::add($err_str, 'message', 'danger');
			}else{
				$post_data = $_POST;
				$res = $this->model->add_user($post_data);
				if($res > 0){
					UE_Message::add('Đăng ký thành công', 'message', 'success');
				}else{
					UE_Message::add('Đăng ký thất bại', 'message', 'danger');
				}
			}
			
		}

		$this->loadView('user/signup');
	}

}