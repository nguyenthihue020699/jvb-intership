<div class="table-book mt-4">
	<h3 class="mb-4">Sách <a href="<?php echo ue_get_admin_link('book','add_Books'); ?>" class="btn btn-info btn-sm float-right">Thêm mới</a></h3>
	<?php
	if(!empty($data)) {
		UE_Message::show('book');
		?>
		<table class="table">
			<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Ảnh</th>
				<th scope="col">Tên</th>
				<th scope="col">Giá</th>
				<th scope="col">Số trang</th>
				<th scope="col">Năm xuất bản</th>
				<th scope="col">Nhà xuất bản</th>
				<th scope="col"></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($data as $k => $v){ ?>
			<tr>
				<th scope="row"><?php echo $v['id']; ?></th>
				<td><?php echo $v['thumb']; ?></td>
				<td><?php echo $v['name']; ?></td>
				<td><?php echo ue_format_price($v['price']); ?></td>
				<td><?php echo $v['number_page']; ?></td>
				<td><?php echo $v['publish_year']; ?></td>
				<td><?php echo $v['publish_author']; ?></td>
				<td>
					<a href="" class="btn btn-info btn-sm">Sửa</a>
					<a href="<?php echo ue_get_admin_link('book', 'delete') . '/' . $v['id']; ?>" class="btn btn-danger btn-sm">Xóa</a>
				</td>
			</tr>
			<?php } ?>
			</tbody>
		</table>
		<?php
	}else{
		?>
		<div class="alert alert-warning">Không có quyển sách nào.</div>
		<?php
	}
	?>
</div>