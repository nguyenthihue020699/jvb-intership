<?php

?>
<form action="" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1"><h5>Ảnh</h5></label>
    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ảnh" name="thumb">
    <label for="exampleInputEmail2"><h5>Tên sách</h5></label>
    <input type="text" class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp" placeholder="Tên sách" name="name">
    <label for="exampleInputEmail3"><h5>Giá</h5></label>
    <input type="text" class="form-control" id="exampleInputEmail3" aria-describedby="emailHelp" placeholder="Giá" name="price">
    <label for="exampleInputEmail4"><h5>Số trang</h5></label>
    <input type="text" class="form-control" id="exampleInputEmail4" aria-describedby="emailHelp" placeholder="Số trang" name="number_page">
    <label for="exampleInputEmail5"><h5>Năm xuất bản</h5></label>
    <input type="text" class="form-control" id="exampleInputEmail5" aria-describedby="emailHelp" placeholder="Năm xuất bản" name="publish_year">
    <label for="exampleInputEmail6"><h5>Nhà xuất bản</h5></label>
    <input type="text" class="form-control" id="exampleInputEmail6" aria-describedby="emailHelp" placeholder="Nhà xuất bản" name="publish_author">
    <label for="exampleInputEmailDanhMUc"><h5>Danh mục</h5></label>
    <!-- <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Danh mục" name="category_id"> -->
    <select name="category_id" id="exampleInputEmailDanhMUc" class="form-control">
      <?php
        if(!empty($data)){
            foreach ($data as $k => $v) {
              ?>
                <option value="<?php echo $v['id']; ?>"> <?php echo $v['name']; ?> </option>
              <?php
            }
        }

      ?>
    </select>
  </br>
     <label for="exampleInputEmail7"><h5>Mô tả</h5></label>
    <input type="text" class="form-control" id="exampleInputEmail7" aria-describedby="emailHelp" placeholder="Mô tả" name="description">
  </div>
  <button type="submit" class="btn btn-primary" name="add_book">Thêm</button>
  <?php UE_Message::show('message'); ?>
</form>