<?php
if(!empty($res)){
	?>
	<div class="list-product">
		<p><a href="#"><?php echo 'Danh sách <span>'. count($res) .'</span> sản phẩm'; ?></a></p>

	</div>
	<?php
	echo '<div class="box">';
	foreach ($res as $key => $value) {
		$link_detail = ue_get_link('book', 'detail') . '/' . $value['id'];
		?>
		<div class="item">
			<div class="section1">
				<a href="<?php echo $link_detail; ?>"><img src="<?php echo SITEURL; ?>assets/<?php echo $value['thumb']; ?>" width="247.5"></a>
			</div>
			<div class="section2">
				<p><a href="<?php echo $link_detail; ?>"><?php echo $value['name']; ?></a></p>
				<p class="price">Giá: <?php echo ue_format_price($value['price']); ?></p>
				<p class="description"><?php echo ue_cut_string($value['description']); ?> </p>
				<p><form action="<?php echo ue_get_link('cart', 'add_cart'); ?>" method="POST">
                    <input type="hidden" name="book_id" value="<?php echo $value['id']; ?>" />
					<button  class="buy-book" name="add_to_cart" value="1">Mua sách</button>
				</form></p>
			</div>
		</div>
		<?php
	}
	echo '</div>';
}else{
	echo 'Không có quyển sách nào.';
}
